﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Sockets;
using System.Text;
using Common;
using SysLog;

//M1	发送/无回答
//M2	发送/确认
//M3	多包发送/确认
//M4	查询/响应


namespace DataAnalysisGPRS
{
    public class GPRS_Hydro_Hex
    {
        Checkout.Check check = new Checkout.Check();
        Model.OrigData orgdata = new Model.OrigData();
        Model.LastData lastdata = new Model.LastData();
        Model.OrigMsg orgmsg = new Model.OrigMsg();
        Model.HydroFrame hydroFrame = new Model.HydroFrame();
        Model.HydroBaseConfig baseConfig = new Model.HydroBaseConfig();
        Model.HydroRunConfig runConfig = new Model.HydroRunConfig();
        Model.HydroAlarmStatus alarmStatus = new Model.HydroAlarmStatus();

        Model.WaterData WaterData = new Model.WaterData();
        Model.RainData RainData = new Model.RainData();

        Model.stPPTN stPpin = new Model.stPPTN();
        Model.stPPTN0 stPpin0 = new Model.stPPTN0();
        Model.stRIVER stRiver = new Model.stRIVER();
        Model.stRSVR stRsvr = new Model.stRSVR();
        Model.stTIDE stTide = new Model.stTIDE();

        DataBase.bll bll;
        SysLog.WriteException writeExcptLog = new SysLog.WriteException();
        CmdHandle cmdHandle = new CmdHandle();
        StringBuilder JsonString = new StringBuilder();

        DataBase.Sqlhelp sqlhelp;

        string numcount;
        string swtype;

        static object locker = new object();

        int frameStartIndex = 14;   //报文起始符下标
        int lenth = 0;             //报文起始符之后、报文结束符之前的报文字节数
        ushort checkCRC16;

        bool recvFinish = false;
        bool reSendFlag = false;

        private static List<Model.stRIVER> G_ZList = new List<Model.stRIVER>();
        private static List<Model.stRSVR> G_RZList = new List<Model.stRSVR>();

        public GPRS_Hydro_Hex(DataBase.Sqlhelp msqlhelp)
        {
            sqlhelp = msqlhelp;
            bll = new DataBase.bll(msqlhelp);
        }

        public void recvHandle(byte[] data, ISession session, ref List<Model.picInfo> G_picFileList)
        {
            int index = 0;
            string ID = "";

            numcount = AppConfigurtaionServices.Configuration["AppConfig:Protocol:NumCount"];
            swtype = AppConfigurtaionServices.Configuration["AppConfig:Protocol:SwType"];
            if (numcount == "") numcount = "8";
            if (swtype == "") swtype = "SL651-2014";

            try
            {
                

            }
            catch (Exception e) when (e.GetType() != typeof(SocketException))
            {
                writeExcptLog.SavaException("GPRS_Hydro_Hex.recvHandle" + e.Message.ToString() + "数据：" + check.byteToHexStr(data));
            }
        }

        //HEX/BCD编码正文解析
        private void UpFrame_Body(byte[] data, int index, int len, ref List<Model.picInfo> G_picFileList, ref List<Model.stRIVER> G_ZList, ref List<Model.stRSVR> G_RZList, string ID)
        {
            Checkout.StationCode scode = new Checkout.StationCode();
            //index初始值为17
            int dataLenth = 0;  //数据字节数
            int decimalPlace = 0;  //小数点后位数
            int paraIndex = 0; //要素信息起始值  

            orgdata.stationID = hydroFrame.stationAddr;
            WaterData.stationID = hydroFrame.stationAddr;
            RainData.stationID = hydroFrame.stationAddr;

            //图片接收
            
            lastdata.STCD = orgdata.stationID;
            lastdata.TM = orgdata.datetime;
            sqlhelp.UpdateLastData(lastdata);

            //--------解析完入标准库------------
            //switch (hydroFrame.stationClass)            //遥测站分类码    
            //{
            //    case "48":             //河道
            //        sqlhelp.AddRiverInfo(stRiver);
            //        break;
            //    case "50":             //降水
            //        sqlhelp.AddPpinInfo(stPpin);
            //        break;
            //    case "4B":             //水库（湖泊）
            //        sqlhelp.AddRsvrInfo(stRsvr);
            //        break;
            //    default:
            //        break;
            //}
        }
    }
}
