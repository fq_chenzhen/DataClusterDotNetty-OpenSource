﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Infrastructure;

namespace DataAnalysisGPRS
{

    //从消息队列提取指令并发送
    //修改指令状态
    class CmdHandle
    {
        Checkout.Check check = new Checkout.Check();
        private DataBase.bll bll;

        public void SendCmd(string sid, ISession session, DataBase.Sqlhelp sqlhelp)
        {
            bll = new DataBase.bll(sqlhelp);
            var dr = bll.SeeUntreatedCmd(sid);

            Console.WriteLine("当前下发指令列表");

            string ip = session.RemoteEndPoint.ToString();

            for (int i = 0; i < dr.Length; i++)
            {
                string atCmd = (string)dr[i][1];

                if (atCmd.Substring(0, 1).Contains("A") || atCmd.Substring(0, 1).Contains("a")) //添加AT指令
                {
                    atCmd = atCmd + "\r\n";
                    var cmd = System.Text.Encoding.Default.GetBytes(atCmd);
                    session.Send(cmd, 0, cmd.Length);
                    Console.WriteLine(dr[i][1].ToString());
                    sqlhelp.UpdateSendCmdStatus(sid, ip, dr[i][0].ToString());
                    return;
                }

                if (atCmd.Substring(0, 2).Contains("7E"))
                {
                    byte[] cmd = check.HexStrToBytes(dr[i][1].ToString()); //只适合水文协议

                    //从消息队列提取指令并发送
                    Array.Copy(check.HexStrToBytes(System.DateTime.Now.ToString("yyMMddHHmmss")), 0, cmd, 16,
                        6); //发报时间   20160315

                    ushort crc16 = check.GetCRC16(cmd, 0, cmd.Length - 2);
                    cmd[cmd.Length - 2] = (byte)(crc16 >> 8); //CRC校验
                    cmd[cmd.Length - 1] = (byte)crc16;

                    //-------------------------------------------------------------------------------------

                    session.Send(cmd, 0, cmd.Length);

                    Console.WriteLine(dr[i][1].ToString());
                    sqlhelp.UpdateSendCmdStatus(sid, ip, dr[i][0].ToString());
                    return;
                }

                if (atCmd.Substring(0, 2).Contains("68"))
                {
                    byte[] cmd = check.HexStrToBytes(dr[i][1].ToString());
                    session.Send(cmd, 0, cmd.Length);
                    //socket.SendTo(cmd, SocketFlags.None, rmtPoint);

                    Console.WriteLine((string)dr[i][1]);
                    sqlhelp.UpdateSendCmdStatus(sid, ip, dr[i][0].ToString());
                    return;
                }

                session.Send(atCmd);
                //socket.SendTo(System.Text.Encoding.Default.GetBytes(atCmd), SocketFlags.None, rmtPoint);


            }

            //修改数据库指令状态为“已处理”
            if (dr.Count() > 0)
            {
                Console.WriteLine(dr[0][1].ToString());
                sqlhelp.UpdateSendCmdStatus(sid, ip, (string)dr[0][0]);
            }
        }
    }
}
