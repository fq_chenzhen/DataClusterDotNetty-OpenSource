﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Data;
using System.Threading.Tasks;
using Infrastructure;
using SysLog;

namespace DataAnalysisGPRS
{
    public class AnalysisGPRS
    {

        DataBase.Sqlhelp sqlhelp = new DataBase.Sqlhelp();

        SysLog.WriteException writeExcptLog = new SysLog.WriteException();

        Model.ReplyCmd Replycmd = new Model.ReplyCmd();
        Model.HydroBaseConfig basecmd = new Model.HydroBaseConfig();
        Model.HydroRunConfig runcmd = new Model.HydroRunConfig();


        List<Model.picInfo> G_picFileList = new List<Model.picInfo>();      //图片文件列表

        //读取配置文件来选取解析协议列表

        //每个协议开启一个子线程来解析
        public bool Analysis(byte[] data, ISession session)
        {
            //GPRS_scsw.recvHandle(data, socket, rmtPoint, ref G_picFileList);
            GPRS_Hydro_Hex GPRS_hydro_hex = new GPRS_Hydro_Hex(sqlhelp);

            GPRS_hydro_hex.recvHandle(data, session, ref G_picFileList);

            //GPRS_water.recvHandle(data, socket, rmtPoint);
            Reply(data, session.RemoteEndPoint);

            sqlhelp.ClusterEqueue();

            return true;
        }

        public void Reply(byte[] data, IPEndPoint rmtPoint)
        {
            if (data[0] == '+')
            {
                string[] recvData;
                string STCD = "";
                string recvStrs = System.Text.Encoding.ASCII.GetString(data);//回复内容

                LogHelper.Warn(recvStrs);

                string ip = $"{rmtPoint.Address.MapToIPv4()}:{rmtPoint.Port}";//ip
                DateTime TM = System.DateTime.Now;//回复时间   

                STCD = sqlhelp.UpdateSendCmd(recvStrs, ip, TM);
                recvData = recvStrs.Split(new char[] { '+' });
                for (int i = 0; i < recvData.GetLength(0); i++)
                {
                    if (recvData[i].Contains("sttyp"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("sttyp", "");
                        basecmd.Sttyp = resultData.Trim();
                    }
                    else if (recvData[i].Contains("chmain") && recvData[i].Contains("1"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chmain", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chmain1 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("chmain") && recvData[i].Contains("2"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chmain", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chmain2 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("chmain") && recvData[i].Contains("3"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chmain", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chmain3 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("chmain") && recvData[i].Contains("4"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chmain", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chmain4 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("chback") && recvData[i].Contains("1"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chback", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chback1 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("chback") && recvData[i].Contains("2"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chback", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chback2 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("chback") && recvData[i].Contains("3"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chback", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chback3 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("chback") && recvData[i].Contains("4"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("chback", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.Chback4 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("workmode"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("workmode", "");
                        basecmd.WorkMode = resultData.Trim();
                    }
                    else if (recvData[i].Contains("sendmode"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("sendmode", "");
                        basecmd.SendMode = resultData.Trim();
                    }
                    else if (recvData[i].Contains("gprsip") && recvData[i].Contains("1"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsip", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterIP1 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsip") && recvData[i].Contains("2"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsip", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterIP2 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsip") && recvData[i].Contains("3"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsip", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterIP3 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsip") && recvData[i].Contains("4"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsip", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterIP4 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsport") && recvData[i].Contains("1"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsport", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterPort1 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsport") && recvData[i].Contains("2"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsport", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterPort2 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsport") && recvData[i].Contains("3"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsport", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterPort3 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsport") && recvData[i].Contains("4"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsport", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        basecmd.CenterPort4 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("gprsapn"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("gprsapn", "");
                        basecmd.GPRSsapn = resultData.Trim();
                    }
                    else if (recvData[i].Contains("sendhour"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("sendhour", "");
                        runcmd.SendHour = resultData.Trim();
                    }
                    else if (recvData[i].Contains("dtmin"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("dtmin", "");
                        runcmd.Dtmin = resultData.Trim();
                    }
                    else if (recvData[i].Contains("dtsend"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("dtsend", "");
                        runcmd.DtSend = resultData.Trim();
                    }
                    else if (recvData[i].Contains("picmin"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("picmin", "");
                        runcmd.Picmin = resultData.Trim();
                    }
                    else if (recvData[i].Contains("rainprm"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("rainprm", "");
                        runcmd.RainResolution = resultData.Trim();
                    }
                    else if (recvData[i].Contains("rainchg"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("rainchg", "");
                        runcmd.RainAddThod = resultData.Trim();
                    }
                    else if (recvData[i].Contains("waterbase") && recvData[i].Contains("1"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("waterbase", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        runcmd.WaterBase1 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("waterbase") && recvData[i].Contains("2"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("waterbase", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        runcmd.WaterBase2 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("watermul") && recvData[i].Contains("1"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("watermul", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        runcmd.Watermul1 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("watermul") && recvData[i].Contains("2"))
                    {
                        string[] recvdt;
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("watermul", "");
                        recvdt = resultData.Split(new char[] { ',' });
                        runcmd.Watermul2 = recvdt[1].Trim();
                    }
                    else if (recvData[i].Contains("waterupchg"))
                    {
                        string resultData = recvData[i].Replace("OK", "").Replace("ok", "").Replace(":", "").Replace("waterupchg", "");
                        runcmd.WaterUpThod = resultData.Trim();
                    }
                    else
                    {
                        continue;
                    }

                    if (string.IsNullOrEmpty(STCD)) return;

                    basecmd.StationID = STCD;
                    basecmd.TM = System.DateTime.Now;
                    runcmd.StationID = STCD;
                    runcmd.TM = basecmd.TM;
                    sqlhelp.UpdateBaseConfig(basecmd);
                    sqlhelp.UpdateRunConfig(runcmd);
                }
            }
        }
    }
}
