# .net Core百万并发Socket通信(UDP、TCP)、及数据解析架构、基于DotNetty

* 使用.net core通用主机搭建框架、windows 服务使用Topshelf组件安装
* 基于DotNetty百万并发Socket组件
* 日志组件使用log4net、数据库操作 Dapper 多线程批量入库
* 添加 心跳包监测，规定时间内没有数据包的tcp服务端自动断开
* 具体的TCP连接数测试相关的限制：
  1. windows、linux 操作系统级别的soket连接数限制、需要改相应的配置文件
  2. 路由器的tcp连接数限制
  3. 测试机的连接数限制，主要表现为：防火墙、杀毒软件
* 测试4核8G内存，3500 TCP长连接并发回复无丢包、数据入库8s（包括最新数据、历史报文、历史数据，每条报文大概7笔sql）



