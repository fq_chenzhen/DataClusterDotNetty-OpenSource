﻿using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Infrastructure.Common;
using SysLog;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Topshelf;

namespace DataCluster.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
           var builder = new HostBuilder()
                .ConfigureHostConfiguration(configHost =>
                {
                    //var coreAssemblyDirectoryPath = Path.GetDirectoryName(typeof(Program).Assembly.Location);
                    //var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                    //var pathToContentRoot = Path.GetDirectoryName(pathToExe);
                    //configHost.SetBasePath(coreAssemblyDirectoryPath);
                })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: true); // Microsoft.Extensions.Configuration.Json
                    //config.AddEnvironmentVariables(); // Microsoft.Extensions.Configuration.EnvironmentVariables

                    //if (args != null)
                    //{
                    //    config.AddCommandLine(args); // Microsoft.Extensions.Configuration.CommandLine
                    //}
                })
                .ConfigureServices((hostContext, services) =>
                {
                    //services.AddSingleton<IHostLifetime, TopshelfLifetime>();
                    services.AddOptions();
                    services.Configure<AppConfig>(hostContext.Configuration.GetSection("AppConfig")); // Microsoft.Extensions.Logging.Configuration
                    services.AddSingleton<IHostedService, TcpAndUdpSocketService>();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    //logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging")); // Microsoft.Extensions.Logging , Microsoft.Extensions.Logging.Configuration
                    //logging.AddConsole(); // Microsoft.Extensions.Logging.Console
                    //logging.AddLog4Net(); // Microsoft.Extensions.Logging.Log4Net.AspNetCore
                });

            //await builder.RunConsoleAsync();

            HostFactory.Run(x =>
            {
                x.Service<IHost>(s =>
                {
                    s.ConstructUsing(() =>builder.Build());
                    s.WhenStarted(service =>
                    {
                        // 授权判断
                        if (!ComputerInfo.IsVerifyCodeAuthed())
                        {
                            return;
                        }

                        service?.Start();
                    });
                    s.WhenStopped(service =>
                    {
                        service?.StopAsync();
                    });
                });

                x.RunAsLocalSystem();

                x.SetDescription("数据汇集平台Service");
                x.SetDisplayName("数据汇集平台Service");
                x.SetServiceName("数据汇集平台Service");
                x.StartAutomaticallyDelayed();
                x.EnableShutdown();

                // 设置服务失败后的操作，分别对应第一次、第二次、后续
                x.EnableServiceRecovery(t =>
                {
                    t.RestartService(0);
                    t.RestartService(1);
                    t.RestartService(10);
                    t.OnCrashOnly();
                });
            });
        }
    }
}
