﻿namespace DataCluster.Console
{
    public class AppConfig
    {
        public SocketServerConf SocketServer { get; set; }

        public DbConf Db { get; set; }

        public ProtocolConf Protocol { get; set; }


        public class SocketServerConf
        {
            public string Ip { get; set; }

            public int ReaderIdleTimeSeconds { get; set; }

            public int Port { get; set; }
        }

        public class DbConf
        {
            public DbConnStrConf DbConnStr { get; set; }

            public DbTypeConf DbType { get; set; }


            public class DbConnStrConf
            {
                public string db1 { get; set; }

                public string db2 { get; set; }

                public string db3 { get; set; }
            }

            public class DbTypeConf
            {
                public string db1 { get; set; }

                public string db2 { get; set; }

                public string db3 { get; set; }
            }
        }

        public class ProtocolConf
        {
            public int NumCount { get; set; }

            public string SwType { get; set; }

            public string ImagePath { get; set; }
        }
    }
}
