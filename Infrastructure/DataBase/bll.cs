﻿using System.Data;

namespace DataBase
{
    public class bll
    {
        Sqlhelp sqlhelp;

        public bll(DataBase.Sqlhelp msqlhelp)
        {
            sqlhelp = msqlhelp;
        }

        /// <summary>
        /// 绑定下拉框
        /// </summary>
        /// <param name="cmdstr"></param>
        /// <returns></returns>
        public DataTable BindCmbData(string cmdstr)
        {

            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            return dt;
        }

        //查看站点参数列表
        public DataTable SeeConfigParameter(string sid)
        {

            string cmdstr = "select Name ,Code ,Type, Value ,Explain from StationPara where AreaID = " + sid + "";

            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            return dt;
        }

        public int GetLastID()
        {
            string cmdstr = "select max(ID) from SendCmd";

            int i = DB.Intance.SqlExecuteScalar(cmdstr);

            return i;
        }

        public DataTable SeeUntreatedCmd()
        {

            string cmdstr = "select CmdString from SendCmd where CmdFlag = '待处理' ";

            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            return dt;
        }

        public DataRow[] SeeUntreatedCmd(string sid)
        {
            //string cmdstr = "select id,CmdString from SendCmd where  StationID ='" + sid + "' and  CmdFlag = '0'";
            //string cmdstr= string.Format("select id,cmdstring from sendcmd where stationid = '{0}' and cmdflag = '0' order by id asc",sid);

            string cmdstr = string.Format("select id, cmdstring, StationID from sendcmd where cmdflag = '0' order by id asc");

            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            var dr = dt.Select($"StationID='{sid}'");
            return dr;
        }

        public DataTable DB_SensorPara(string sid, int num)
        {

            string cmdstr = "select A,B,C,K,F0,T0 from DB_SensorPara where  STCD ='" + sid + "' and item='" + num + "'";

            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            return dt;
        }

        #region 查询实时水雨晴标准库表

        public DataTable SeeRiverInfo(Model.stRIVER river)
        {
            string cmdstr = "select STCD ,TM ,Z ,Q ,XSA ,XSAVV ,XSMXV ,FLWCHRCD ,WPTN ,MSQMT ,MSAMT ,MSVMT from ST_RIVER_R where 1=1";

            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            return dt;
        }

        //水库水情表
        public DataTable SeeRsvrInfo(Model.stRSVR rsvr)
        {
            string cmdstr = "select STCD ,TM ,RZ ,INQ ,W,BLRZ,OTQ,RWCHRCD,RWPIN,INQDR,MSQMT from ST_RSVR_R where 1=1";

            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            return dt;
        }

        //降水量表
        public DataTable SeePpinInfo(Model.stPPTN ppin)
        {
            string cmdstr = "select STCD ,TM ,DRP ,INTV ,PDR ,DYP ,WTH from ST_PPIN_R where 1=1";
            DataTable dt = sqlhelp.BindGridViewData(cmdstr);

            return dt;
        }

        #endregion

        #region 配置信息

        //public string UpdateBaseConfig(Model.HydroBaseConfig baseConfig)
        //{
        //    string result = "";

        //    int i = sqlhelp.UpdateBaseConfig(baseConfig);

        //    if (i > 0)
        //    {
        //        result = "true";

        //    }
        //    else
        //    {
        //        result = "修改失败请重试！！";
        //    }
        //    return result;
        //}

        //public string UpdateRunConfig(Model.HydroRunConfig runConfig)
        //{
        //    string result = "";

        //    int i = sqlhelp.UpdateRunConfig(runConfig);

        //    if (i > 0)
        //    {
        //        result = "true";

        //    }
        //    else
        //    {
        //        result = "修改失败请重试！！";
        //    }
        //    return result;
        //}


        #endregion
    }
}
