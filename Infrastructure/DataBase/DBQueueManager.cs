﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataBase
{
    public static class DBQueueManager
    {
        private static List<Thread> _threads = new List<Thread>();
        public static void Start()
        {
            // 开线程通过队列数据入库
            //for (var i = 0; i < dbThreads; i++)
            //{
            Thread oThread1 = new Thread(() =>
            {
                DB.ExecuteQueue1();
            });
            oThread1.IsBackground = true;
            //oThread1.Priority = ThreadPriority.AboveNormal;
            oThread1.Start();

            Thread oThread2 = new Thread(() =>
            {
                DB.ExecuteQueue2();
            });
            oThread2.IsBackground = true;
            oThread2.Start();

            Thread oThread3 = new Thread(() =>
            {
                DB.ExecuteQueue3();
            });
            oThread3.IsBackground = true;
            oThread3.Start();

            _threads.Add(oThread1);
            _threads.Add(oThread2);
            _threads.Add(oThread3);
            //}
        }

        public static void Stop()
        {
            _threads.ForEach(t =>
            {
                //if (t.IsAlive)
                //    t.Interrupt();
            });
        }
    }
}
