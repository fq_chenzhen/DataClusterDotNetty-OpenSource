﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;

namespace DataBase
{
    public class LocalCache
    {
        static readonly IMemoryCache Cache = new MemoryCache(new MemoryCacheOptions());
        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <returns>Cached item as type</returns>
        public static T Get<T>(string key) where T : class
        {
            try
            {
                return (T)Cache.Get(key);
            }
            catch
            {
                return null;
            }
        }

        public static T Get<T>(string key, Func<T> getValue, int cacheMinute = 10) where T : class
        {
            try
            {
                if (LocalCache.Exists(key))
                {
                    return (T)Cache.Get(key);
                }
                else
                {
                    var v = getValue();
                    LocalCache.Add(v, key, cacheMinute);
                    return v;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add<T>(T objectToCache, string key) where T : class
        {
            Cache.Set(key, objectToCache, DateTime.Now.AddDays(7));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Set<T>(T objectToCache, string key) where T : class
        {
            Cache.Set(key, objectToCache, DateTime.Now.AddDays(7));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        /// <param name="cacheSecond">缓存的秒数</param>
        public static void Add<T>(T objectToCache, string key, int cacheSecond = 7200) where T : class
        {
            Cache.Set(key, objectToCache, new DateTimeOffset(DateTime.Now.AddSeconds(cacheSecond)));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add(object objectToCache, string key)
        {
            Cache.Set(key, objectToCache, DateTime.Now.AddDays(7));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        /// <param name="cacheMinute">缓存多少分钟</param>
        public static void Add(object objectToCache, string key, int cacheMinute = 30)
        {
            Cache.Set(key, objectToCache, DateTime.Now.AddMinutes(cacheMinute));
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            object obj;
            if (Cache.TryGetValue(key, out obj)) return true;
            return false;
        }


        public static void Remove(string key)
        {
            if (LocalCache.Exists(key))
                 Cache.Remove(key);
        }
    }
}