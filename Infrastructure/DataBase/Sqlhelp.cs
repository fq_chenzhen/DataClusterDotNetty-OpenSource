﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DataBase
{
    public class Sqlhelp
    {
        
        private static readonly Random _random = new Random();

        private DB db = new DB();


        private bool _isFirstAddOrigData = true;

        /// <summary>
        /// 获得DRIDVIEW的数据源
        /// </summary>
        /// <param name="cmdstr"></param>
        /// <returns></returns>
        public DataTable BindGridViewData(string cmdstr)
        {
            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 1);
            return dt;
        }

        public int Bind(string cmdstr)
        {
            int dt = db.SqlExecute(cmdstr);
            return dt;
        }

        #region 原始数据入原始表
        public void AddOrigData(Model.OrigData data) //数据存入基础数据表中
        {
            float? waterBase = 0;
            float? k = 1.0f;//水位系数

            if (data.data == 9.999f)
                data.data = 0;

            if (data.dataType == "Z" || data.dataType == "Z01" || data.dataType == "ZN05")     //减去水位基值
            {
                //DataTable dt = db.DataWorke("select DTPR,water1k from ST_STBPRP_B where STCD ='" + data.stationID + "' ");

                var dtpr = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTPR");
                var water1k = (float?)this.getStbprpTbCondition<double?>(data.stationID, "water1k");

                waterBase = dtpr ?? waterBase;
                k = water1k ?? k;

                data.data = data.data + waterBase.Value;
                data.data = data.data * k.Value;

            }
            else
            if (data.dataType == "Z02" || data.dataType == "ZN05")     //减去水位基值
            {
                //DataTable dt = db.DataWorke("select DTPR2,water2k from ST_STBPRP_B where STCD ='" + data.stationID + "' ");

                waterBase = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTPR2") ?? waterBase;
                k = (float?)this.getStbprpTbCondition<double?>(data.stationID, "water2k") ?? k;

                data.data = data.data + waterBase.Value;
                data.data = data.data * k.Value;
            }
            else
            if (data.dataType == "Z03" || data.dataType == "ZN05")     //减去水位基值
            {
                //DataTable dt = db.DataWorke("select DTPR3,water3k from ST_STBPRP_B where STCD ='" + data.stationID + "' ");

                waterBase = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTPR3") ?? waterBase;
                k = (float?)this.getStbprpTbCondition<double?>(data.stationID, "water3k") ?? k;

                data.data = data.data + waterBase.Value;
                data.data = data.data * k.Value;
            }
            else
            if ((data.dataType == "TFS1") | (data.dataType == "FS1") | (data.dataType == "A1"))
            {
                //DataTable dt = db.DataWorke("select flow1k from ST_STBPRP_B where STCD ='" + data.stationID + "' ");

                k = (float?)this.getStbprpTbCondition<double?>(data.stationID, "flow1k") ?? k;
                data.data = data.data * k.Value;
            }

            // 数据集合批量插入
            string cmdstr;
            if (_isFirstAddOrigData)
            {
                cmdstr = "insert into OriginalData(MsgID,StationID,FacNum,DataType,Data,DataTime,Chanal) values (" + data.MsgID + ",'" + data.stationID + "','" + data.FacNum + "','" + data.dataType + "'," + data.data + ",'" + data.datetime + "','" + data.chanal + "')";
                _isFirstAddOrigData = false;
            }
            else
            {
                cmdstr = ",(" + data.MsgID + ",'" + data.stationID + "','" + data.FacNum + "','" + data.dataType + "'," + data.data + ",'" + data.datetime + "','" + data.chanal + "')";
            }
            db.SqlExecute(cmdstr, true);
        }

        public void AddZCData(Model.OrigData data) //将召测数据存入召测数据表中
        {
            float? waterBase = 0;
            //float waterData = 0;

            if (data.dataType == "Z" || data.dataType == "Z01")     //减去水位基值
            {
                //DataTable dt = db.DataWorke("select DTPR from ST_STBPRP_B where STCD ='" + data.stationID + "' ");

                waterBase = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTPR") ?? waterBase;

                data.data = data.data + waterBase.Value;
            }
            if (data.dataType == "Z02")     //减去水位基值
            {
                //DataTable dt = db.DataWorke("select DTPR2 from ST_STBPRP_B where STCD ='" + data.stationID + "' ");
                waterBase = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTPR2") ?? waterBase;

                data.data = data.data + waterBase.Value;
            }
            if (data.dataType == "Z03")     //减去水位基值
            {
                //DataTable dt = db.DataWorke("select DTPR3 from ST_STBPRP_B where STCD ='" + data.stationID + "' ");
                waterBase = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTPR3") ?? waterBase;

                data.data = data.data + waterBase.Value;
            }
            string cmdstr = "insert into ZCData(StationID,DataType,Data,DataTime,Chanal) values('" + data.stationID + "','" + data.dataType + "','" + data.data + "','" + data.datetime + "','" + data.chanal + "')";

            int i = db.SqlExecute(cmdstr);
        }

        public string AddOrigMsg(Model.OrigMsg msg)
        {
            string cmdstr = "";
            var msgId = $"@msgId{_random.Next(int.MaxValue)}";

            cmdstr += " insert into OriginalMsgGPRS(FacNum,StationID,Message,RcvTime,IP) values('" + msg.FacNum + "','" + msg.stationID + "','" + msg.msg + "','" + msg.recvTime + "','" + msg.ip + "');";
            if (db.DbType == "MySql")
            {
                cmdstr += $"set {msgId}= LAST_INSERT_ID()  ";
            }
            else
            {
                cmdstr += $"declare {msgId} int; set {msgId}= scope_identity() ";
            }

            int i = db.SqlExecute(cmdstr);

            return msgId;
        }

        #endregion

        #region 下发更新指令
        // 因要获取stcd, 不加入todoActions
        public string UpdateSendCmd(string reply, string ip, DateTime tm)
        {
            DateTime etm = tm.AddSeconds(-1);
            string STCD = "";

            string cmdstr = "update SendCmd set Reply='" + reply + "' where ID=( select ID from SendCmd where ProcessTime<='" + tm + "' and ProcessTime>'" + etm + "' and IP='" + ip + "')";
            int i = db.SqlExecute(cmdstr);

            string relystr = "select StationID from SendCmd where ID=( select top 1 ID from SendCmd where ProcessTime<='" + tm + "' and ProcessTime>'" + etm + "' and IP='" + ip + "')";
            DataTable dt = db.DataWorke(relystr);

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0] != null)
                {
                    STCD = dt.Rows[0][0].ToString();
                }
            }

            return STCD;
        }

        [Obsolete]
        public int AddSendCmd(string id, string cmd)
        {
            string cmdstr = "insert into SendCmd(StationID ,CmdString ,SubTime ,Chanal ,CmdFlag) values('" + id + "','" + cmd + "','" + System.DateTime.Now + "','GPRS' , '0')";
            cmdstr = cmdstr.Replace("'null'", "null");
            int i = db.SqlExecute(cmdstr);

            return i;
        }

        public void UpdateSendCmdStatus(string sid, string ip, string id)
        {
            string cmdstr = "update SendCmd set ProcessTime='" + System.DateTime.Now.ToString()
                + "', CmdFlag= '1',IP='" + ip + "' where ID = " + id;

            int i = db.SqlExecute(cmdstr);
        }
        #endregion

        #region 入实时水雨晴标准库表

        //水库水情表

        public void AddRiverInfo(Model.stRIVER river)
        {
            string cmdstr = "insert into ST_RIVER_R(STCD ,TM ,Z ,Q ,XSA ,XSAVV ,XSMXV) values('" + river.stcd + "','" + river.tm + "','" + river.z + "','" + river.q + "','" + river.xsa + "','" + river.xsavv + "','" + river.xsmxv + "')";
            cmdstr = cmdstr.Replace("'null'", "null");


            int i = db.SqlExecute2(cmdstr);
            int j = db.SqlExecute3(cmdstr);
        }

        //水库水情表
        public void AddRsvrInfo(Model.stRSVR rsvr)
        {
            string cmdstr = "insert into ST_RSVR_R(STCD ,TM ,RZ ,INQ ,W,BLRZ,OTQ,RWCHRCD,RWPTN,INQDR,MSQMT ) values('" + rsvr.stcd + "','" + rsvr.tm + "','" + rsvr.rz + "','" + rsvr.inq + "' ,'" + rsvr.w + "' ,'" + rsvr.blrz + "','" + rsvr.otq + "','" + rsvr.rwchrcd + " ','" + rsvr.rwpin + " ','" + rsvr.inqdr + "','" + rsvr.msqmt + "')";

            cmdstr = cmdstr.Replace("'null'", "null");


            int i = db.SqlExecute2(cmdstr);
            int j = db.SqlExecute3(cmdstr);
        }

        //潮位表
        public void AddTideInfo(Model.stTIDE tide)
        {
            string cmdstr = "insert into ST_TIDE_R(STCD ,TM ,TDZ ,AIRP ,TDCHRCD,TDPTN,HLTDMK) values('" + tide.stcd + "','" + tide.tm + "','" + tide.tdz + "','" + tide.airp + "' ,'" + tide.tdchrcd + "' ,'" + tide.tdptn + "','" + tide.hltdmk + "')";

            cmdstr = cmdstr.Replace("'null'", "null");


            int i = db.SqlExecute2(cmdstr);
            int j = db.SqlExecute3(cmdstr);
        }

        //降水量表
        public void AddPpinInfo(Model.stPPTN ppin)
        {
            string cmdstr = "insert into ST_PPIN_R(STCD ,TM ,DRP ,INTV ,PDR ,DYP ,WTH) values('" + ppin.stcd + "','"
                + ppin.tm + "'," + ppin.drp + "," + ppin.intv + "," + ppin.pdr + "," + ppin.dyp + " , '" + ppin.wth + "')";

            cmdstr = cmdstr.Replace("'null'", "null");

            int i = db.SqlExecute2(cmdstr);
            int ji = db.SqlExecute3(cmdstr);
        }

        //时段降水量   1小时5分钟降水量
        public void AddDRPData(Model.RainData data)
        {
            string cmdstr = "";
            float Extremum = 0;

            DateTime TM = DateTime.ParseExact(data.datetime.ToString("yyMMddHH"), "yyMMddHH", System.Globalization.CultureInfo.CurrentCulture);

            if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 != -1 && data.data10 != -1 && data.data11 != -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05 + data.data06 + data.data07 + data.data08 + data.data09 + data.data10 + data.data11;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "',Data10 = '" + data.data10 + "',Data11 = '" + data.data11 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "','" + data.data10 + "','" + data.data11 + "',NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 != -1 && data.data10 != -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05 + data.data06 + data.data07 + data.data08 + data.data09 + data.data10;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "',Data10 = '" + data.data10 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "','" + data.data10 + "',NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 != -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05 + data.data06 + data.data07 + data.data08 + data.data09;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "',NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05 + data.data06 + data.data07 + data.data08;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "',NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05 + data.data06 + data.data07;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "',NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05 + data.data06;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "',NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 == -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02 + data.data03;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 == -1 && data.data04 == -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01 + data.data02;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 == -1 && data.data03 == -1 && data.data04 == -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                Extremum = data.data01;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else
            {
                Extremum = data.data01 + data.data02 + data.data03 + data.data04 + data.data05 + data.data06 + data.data07 + data.data08 + data.data09 + data.data10 + data.data11 + data.data12;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "',Data10 = '" + data.data10 + "',Data11 = '" + data.data11 + "',Data12 = '" + data.data12 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "','" + data.data10 + "','" + data.data11 + "','" + data.data12 + "')";
                }
            }



            int i = db.SqlExecute(cmdstr);
            int j = db.SqlExecute2(cmdstr);
            int z = db.SqlExecute3(cmdstr);
        }
        public bool SiteDataExist(string stcd, DateTime TM, string Type)
        {
            bool result = false;

            string cmdstr = "select StationID,RcvTime,Type from SiteData where StationID ='" + stcd + "' and RcvTime='" + TM + "' and Type='" + Type + "'";

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 30);

            if (dt.Rows.Count > 0)
            {
                result = true;
            }
            return result;

        }

        //时段相对水位   1小时5分钟相对水位
        public void AddDRZData(Model.WaterData data)
        {
            float? waterData = 0;//高程
            float? waterBase = 0;//高程修正值
            float Extremum = 0;

            string cmdstr = "";

            //DataTable dt = db.DataWorke("select DTMEL,DTPR from ST_STBPRP_B where STCD ='" + data.stationID + "' ");

            waterBase = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTPR") ?? waterBase;
            waterData = (float?)this.getStbprpTbCondition<decimal?>(data.stationID, "DTMEL") ?? waterData;

            //Console.WriteLine("减去水位基值！");

            var waterSumDataAndBase = waterBase.Value + waterData.Value;

            if (data.data01 != -1)
            {
                data.data01 = data.data01 + waterSumDataAndBase;
            }
            if (data.data02 != -1)
            {
                data.data02 = data.data02 + waterSumDataAndBase;
            }
            if (data.data03 != -1)
            {
                data.data03 = data.data03 + waterSumDataAndBase;
            }
            if (data.data04 != -1)
            {
                data.data04 = data.data04 + waterSumDataAndBase;
            }
            if (data.data05 != -1)
            {
                data.data05 = data.data05 + waterSumDataAndBase;
            }
            if (data.data06 != -1)
            {
                data.data06 = data.data06 + waterSumDataAndBase;
            }
            if (data.data07 != -1)
            {
                data.data07 = data.data07 + waterSumDataAndBase;
            }
            if (data.data08 != -1)
            {
                data.data08 = data.data08 + waterSumDataAndBase;
            }
            if (data.data09 != -1)
            {
                data.data09 = data.data09 + waterSumDataAndBase;
            }
            if (data.data10 != -1)
            {
                data.data10 = data.data10 + waterSumDataAndBase;
            }
            if (data.data11 != -1)
            {
                data.data11 = data.data11 + waterSumDataAndBase;
            }
            if (data.data12 != -1)
            {
                data.data12 = data.data12 + waterSumDataAndBase;
            }

            DateTime TM = DateTime.ParseExact(data.datetime.ToString("yyMMddHH"), "yyMMddHH", System.Globalization.CultureInfo.CurrentCulture);


            //if (SiteDataExist(data.stationID, TM, data.dataType))
            //{
            //    cmdstr = "update SiteData set Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "',Data10 = '" + data.data10 + "',Data11 = '" + data.data11 + "',Data12 = '" + data.data12 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
            //}
            //else
            //{
            //    cmdstr = "insert into SiteData(StationID,RcvTime,Type,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.stationID + "','" + TM + "','" + data.dataType + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "','" + data.data10 + "','" + data.data11 + "','" + data.data12 + "')";
            //}

            if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 != -1 && data.data10 != -1 && data.data11 != -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05, data.data06, data.data07, data.data08, data.data09, data.data10, data.data11 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "',Data10 = '" + data.data10 + "',Data11 = '" + data.data11 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "','" + data.data10 + "','" + data.data11 + "',NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 != -1 && data.data10 != -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05, data.data06, data.data07, data.data08, data.data09, data.data10 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "',Data10 = '" + data.data10 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "','" + data.data10 + "',NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 != -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05, data.data06, data.data07, data.data08, data.data09 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "',NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 != -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05, data.data06, data.data07, data.data08 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "',NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 != -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05, data.data06, data.data07 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "',NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 != -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05, data.data06 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "',NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 != -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 != -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 != -1 && data.data04 == -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 != -1 && data.data03 == -1 && data.data04 == -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                float[] maxdata = new float[] { data.data01, data.data02 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else if (data.data01 != -1 && data.data02 == -1 && data.data03 == -1 && data.data04 == -1 && data.data05 == -1 && data.data06 == -1 && data.data07 == -1 && data.data08 == -1 && data.data09 == -1 && data.data10 == -1 && data.data11 == -1 && data.data12 == -1)
            {
                //float[] maxdata = new float[] { };
                Extremum = data.data01;

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
                }
            }
            else
            {
                float[] maxdata = new float[] { data.data01, data.data02, data.data03, data.data04, data.data05, data.data06, data.data07, data.data08, data.data09, data.data10, data.data11, data.data12 };
                Extremum = maxdata.Max();

                if (SiteDataExist(data.stationID, TM, data.dataType))
                {
                    cmdstr = "update SiteData set Extremum = '" + Extremum + "', Data01 = '" + data.data01 + "',Data02 = '" + data.data02 + "',Data03 = '" + data.data03 + "',Data04 = '" + data.data04 + "',Data05 = '" + data.data05 + "',Data06 = '" + data.data06 + "',Data07 = '" + data.data07 + "',Data08 = '" + data.data08 + "',Data09 = '" + data.data09 + "',Data10 = '" + data.data10 + "',Data11 = '" + data.data11 + "',Data12 = '" + data.data12 + "' where StationID = '" + data.stationID + "' and RcvTime = '" + TM + "' and Type = '" + data.dataType + "'";
                }
                else
                {
                    cmdstr = "insert into SiteData(FacNum,StationID,RcvTime,Type,Extremum,Data01,Data02,Data03,Data04,Data05,Data06,Data07,Data08,Data09,Data10,Data11,Data12) values('" + data.FacNum + "','" + data.stationID + "','" + TM + "','" + data.dataType + "','" + Extremum + "'," + data.data01 + ",'" + data.data02 + "','" + data.data03 + "','" + data.data04 + "','" + data.data05 + "','" + data.data06 + "','" + data.data07 + "','" + data.data08 + "','" + data.data09 + "','" + data.data10 + "','" + data.data11 + "','" + data.data12 + "')";
                }
            }

            int i = db.SqlExecute(cmdstr);
            int j = db.SqlExecute2(cmdstr);
            int z = db.SqlExecute3(cmdstr);
        }

        public float DataDYP(DateTime TM, string STCD)
        {
            float DYPdata = 0;

            DateTime yesTM = TM.AddDays(-1);

            string cmdstr = "SELECT STCD,TM,DRP FROM ST_PPTN_R where STCD='" + STCD + "' and TM>'" + yesTM + "' and TM<='" + TM + "' order by TM desc";

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke2(cmdstr), 1);

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i][2] != null) && (dt.Rows[i][2] != DBNull.Value))
                    {
                        string rain = dt.Rows[i][2].ToString();
                        float raindata = float.Parse(rain);

                        DYPdata += raindata;
                    }
                }
            }

            return DYPdata;
        }

        //降水量表
        public void AddPptnInfo(Model.stPPTN ppin)
        {
            string StationID = ppin.stcd;//querySTCD(ppin.stcd);
            string cmdstr = "";

            DateTime TM = DateTime.ParseExact(ppin.tm.ToString("yyMMddHH"), "yyMMddHH", System.Globalization.CultureInfo.CurrentCulture);


            if (ppin.tm.Hour == 8 && ppin.tm.Minute == 0)
            {
                cmdstr = "insert into ST_PPTN_R(STCD ,TM ,DRP ,INTV ,DYP ,WTH) values('" + StationID + "','"
                + TM + "','" + ppin.drp + "','" + ppin.intv + "','" + ppin.dyp + "', '" + ppin.wth + "')";
            }
            else
            {
                cmdstr = "insert into ST_PPTN_R(STCD ,TM ,DRP ,INTV ,WTH) values('" + StationID + "','"
                + ppin.tm + "','" + ppin.drp + "','" + ppin.intv + "', '" + ppin.wth + "')";
            }

            cmdstr = cmdstr.Replace("'null'", "null");

            int i = db.SqlExecute2(cmdstr);
            int j = db.SqlExecute3(cmdstr);

        }

        //五分钟降水量表
        public void AddPptnInfo0(Model.stPPTN0 ppin0)
        {
            string StationID = ppin0.stcd;//querySTCD(ppin.stcd);
            string cmdstr = "";

            DateTime TM = DateTime.ParseExact(ppin0.tm.ToString("yyMMddHH"), "yyMMddHH", System.Globalization.CultureInfo.CurrentCulture);

            cmdstr = "insert into ST_PPTN_R0(stcd ,tm ,val ,sub_val ) values('" + StationID + "','" + ppin0.tm + "','" + ppin0.val + "','" + ppin0.sub_val + "')";

            cmdstr = cmdstr.Replace("'null'", "null");

            int i = db.SqlExecute2(cmdstr);
            int j = db.SqlExecute3(cmdstr);
        }

        private static object _lockStbprpObj = new object();
        private TF getStbprpTbCondition<TF>(string stcd, string Field)
        {
            string cmdstr = "SELECT * FROM ST_STBPRP_B";
            var dt = LocalCache.Get(cmdstr, () =>
            {
                DataTable dt1;
                lock (_lockStbprpObj)
                {
                    dt1 = LocalCache.Exists(cmdstr) ? LocalCache.Get<DataTable>(cmdstr) : db.DataWorke(cmdstr);
                }
                return dt1;
            }, 15);
            var dr = dt.Select($"STCD='{stcd}'");
            return dr.Select(t => t.Field<TF>(Field)).FirstOrDefault();
        }

        public string SiteType(string STCD)
        {
            string type = "";

            //string cmdstr = "SELECT STCD,STTP FROM ST_STBPRP_B where STCD='" + STCD + "'";
            type = this.getStbprpTbCondition<string>(STCD, "STTP");

            if (string.IsNullOrWhiteSpace(type))
            {
                type = "-1";
            }

            return type;
        }

        public float WaterData(string STCD)
        {
            float waterdata = 0;
            waterdata = (float?)this.getStbprpTbCondition<decimal?>(STCD, "DTMEL") ?? waterdata;

            return waterdata;
        }

        #endregion

        #region  遥测站基本、运行配置参数

        public void UpdateBaseConfig(Model.HydroBaseConfig bc)
        {
            string cmdstr = "";
            if (db.DbType == "MySql")
            {
                cmdstr += " insert into BaseConfig(StationID ,TM,CenterAddr ,StationAddr ,Pwd ,MainChanlAddr1 ,BackChanlAddr1,MainChanlAddr2,BackChanlAddr2,MainChanlAddr3,BackChanlAddr3,MainChanlAddr4,BackChanlAddr4,WorkMode,CollectCode,SvrAddrRang,CommDevID,Sttyp,SendMode,Chmain1,Chmain2,Chmain3,Chmain4,Chback1,Chback2,Chback3,Chback4,GPRSsapn,CenterIP1,CenterIP2,CenterIP3,CenterIP4,CenterPort1,CenterPort2,CenterPort3,CenterPort4) values('" + ModelItemIsNull(bc.StationID) + "','" + ModelItemIsNull(bc.TM) + "','" + ModelItemIsNull(bc.CenterAddr) + "','" + ModelItemIsNull(bc.StationAddr) + "','" + ModelItemIsNull(bc.Pwd) + "','" + ModelItemIsNull(bc.MainChanlAddr1) + "' ,'" + ModelItemIsNull(bc.BackChanlAddr1) + "' ,'" + ModelItemIsNull(bc.MainChanlAddr2) + "','" + ModelItemIsNull(bc.BackChanlAddr2) + "','" + ModelItemIsNull(bc.MainChanlAddr3) + "','" + ModelItemIsNull(bc.BackChanlAddr3) + "','" + ModelItemIsNull(bc.MainChanlAddr4) + "','" + ModelItemIsNull(bc.BackChanlAddr4) + "','" + ModelItemIsNull(bc.WorkMode) + "','" + ModelItemIsNull(bc.CollectCode) + "','" + ModelItemIsNull(bc.SvrAddrRang) + "','" + ModelItemIsNull(bc.CommDevID) + "','" + ModelItemIsNull(bc.Sttyp) + "','" + ModelItemIsNull(bc.SendMode) + "','" + ModelItemIsNull(bc.Chmain1) + "','" + ModelItemIsNull(bc.Chmain2) + "','" + ModelItemIsNull(bc.Chmain3) + "','" + ModelItemIsNull(bc.Chmain4) + "','" + ModelItemIsNull(bc.Chback1) + "','" + ModelItemIsNull(bc.Chback2) + "','" + ModelItemIsNull(bc.Chback3) + "','" + ModelItemIsNull(bc.Chback4) + "','" + ModelItemIsNull(bc.GPRSsapn) + "','" + ModelItemIsNull(bc.CenterIP1) + "','" + ModelItemIsNull(bc.CenterIP2) + "','" + ModelItemIsNull(bc.CenterIP3) + "','" + ModelItemIsNull(bc.CenterIP4) + "','" + ModelItemIsNull(bc.CenterPort1) + "','" + ModelItemIsNull(bc.CenterPort2) + "','" + ModelItemIsNull(bc.CenterPort3) + "','" + ModelItemIsNull(bc.CenterPort4) + "')";
                cmdstr += " on duplicate key update ";
                cmdstr += " StationID ='" + ModelItemIsNull(bc.StationID) + "'";
                if (bc.TM != null)
                {
                    cmdstr += ",TM='" + ModelItemIsNull(bc.TM) + "'";
                }
                if (bc.CenterAddr != null)
                {
                    cmdstr += ", CenterAddr='" + ModelItemIsNull(bc.CenterAddr) + "'";
                }
                if (bc.StationAddr != null)
                {
                    cmdstr += ", StationAddr='" + ModelItemIsNull(bc.StationAddr) + "'";
                }
                if (bc.Pwd != null)
                {
                    cmdstr += ",Pwd='" + ModelItemIsNull(bc.Pwd) + "'";
                }
                if (bc.MainChanlAddr1 != null)
                {
                    cmdstr += ", MainChanlAddr1='" + ModelItemIsNull(bc.MainChanlAddr1) + "'";
                }
                if (bc.BackChanlAddr1 != null)
                {
                    cmdstr += ", BackChanlAddr1='" + ModelItemIsNull(bc.BackChanlAddr1) + "'";
                }
                if (bc.MainChanlAddr2 != null)
                {
                    cmdstr += ", MainChanlAddr2='" + ModelItemIsNull(bc.MainChanlAddr2) + "'";
                }
                if (bc.BackChanlAddr2 != null)
                {
                    cmdstr += ", BackChanlAddr2='" + ModelItemIsNull(bc.BackChanlAddr2) + "'";
                }
                if (bc.MainChanlAddr3 != null)
                {
                    cmdstr += ", MainChanlAddr3='" + ModelItemIsNull(bc.MainChanlAddr3) + "'";
                }
                if (bc.BackChanlAddr3 != null)
                {
                    cmdstr += ", BackChanlAddr3='" + ModelItemIsNull(bc.BackChanlAddr3) + "'";
                }
                if (bc.MainChanlAddr4 != null)
                {
                    cmdstr += ", MainChanlAddr4='" + ModelItemIsNull(bc.MainChanlAddr4) + "'";
                }
                if (bc.BackChanlAddr4 != null)
                {
                    cmdstr += ", BackChanlAddr4='" + ModelItemIsNull(bc.BackChanlAddr4) + "'";
                }
                if (bc.WorkMode != null)
                {
                    cmdstr += ", WorkMode='" + ModelItemIsNull(bc.WorkMode) + "'";
                }
                if (bc.CollectCode != null)
                {
                    cmdstr += ", CollectCode='" + ModelItemIsNull(bc.CollectCode) + "'";
                }
                if (bc.SvrAddrRang != null)
                {
                    cmdstr += ", SvrAddrRang='" + ModelItemIsNull(bc.SvrAddrRang) + "'";
                }
                if (bc.CommDevID != null)
                {
                    cmdstr += ", CommDevID='" + ModelItemIsNull(bc.CommDevID) + "'";
                }
                if (bc.Sttyp != null)
                {
                    cmdstr += ", Sttyp='" + ModelItemIsNull(bc.Sttyp) + "'";
                }
                if (bc.SendMode != null)
                {
                    cmdstr += ", SendMode='" + ModelItemIsNull(bc.SendMode) + "'";
                }
                if (bc.Chmain1 != null)
                {
                    cmdstr += ", Chmain1='" + ModelItemIsNull(bc.Chmain1) + "'";
                }
                if (bc.Chmain2 != null)
                {
                    cmdstr += ", Chmain2='" + ModelItemIsNull(bc.Chmain2) + "'";
                }
                if (bc.Chmain3 != null)
                {
                    cmdstr += ", Chmain3='" + ModelItemIsNull(bc.Chmain3) + "'";
                }
                if (bc.Chmain4 != null)
                {
                    cmdstr += ", Chmain4='" + ModelItemIsNull(bc.Chmain4) + "'";
                }
                if (bc.Chback1 != null)
                {
                    cmdstr += ", Chback1='" + ModelItemIsNull(bc.Chback1) + "'";
                }
                if (bc.Chback2 != null)
                {
                    cmdstr += ", Chback2='" + ModelItemIsNull(bc.Chback2) + "'";
                }
                if (bc.Chback3 != null)
                {
                    cmdstr += ", Chback3='" + ModelItemIsNull(bc.Chback3) + "'";
                }
                if (bc.Chback4 != null)
                {
                    cmdstr += ", Chback4='" + ModelItemIsNull(bc.Chback4) + "'";
                }
                if (bc.GPRSsapn != null)
                {
                    cmdstr += ", GPRSsapn='" + ModelItemIsNull(bc.GPRSsapn) + "'";
                }
                if (bc.CenterIP1 != null)
                {
                    cmdstr += ", CenterIP1='" + ModelItemIsNull(bc.CenterIP1) + "'";
                }
                if (bc.CenterIP2 != null)
                {
                    cmdstr += ", CenterIP2='" + ModelItemIsNull(bc.CenterIP2) + "'";
                }
                if (bc.CenterIP3 != null)
                {
                    cmdstr += ", CenterIP3='" + ModelItemIsNull(bc.CenterIP3) + "'";
                }
                if (bc.CenterIP4 != null)
                {
                    cmdstr += ", CenterIP4='" + ModelItemIsNull(bc.CenterIP4) + "'";
                }
                if (bc.CenterPort1 != null)
                {
                    cmdstr += ", CenterPort1='" + ModelItemIsNull(bc.CenterPort1) + "'";
                }
                if (bc.CenterPort2 != null)
                {
                    cmdstr += ", CenterPort2='" + ModelItemIsNull(bc.CenterPort2) + "'";
                }
                if (bc.CenterPort3 != null)
                {
                    cmdstr += ", CenterPort3='" + ModelItemIsNull(bc.CenterPort3) + "'";
                }
                if (bc.CenterPort4 != null)
                {
                    cmdstr += ", CenterPort4='" + ModelItemIsNull(bc.CenterPort4) + "'";
                }
            }
            else
            {
                cmdstr += $"if exists(select 1 from BaseConfig where StationID='{bc.StationID}') ";
                cmdstr += " begin ";
                cmdstr += " update BaseConfig set StationID ='" + ModelItemIsNull(bc.StationID) + "'";
                if (bc.TM != null)
                {
                    cmdstr += ",TM='" + ModelItemIsNull(bc.TM) + "'";
                }
                if (bc.CenterAddr != null)
                {
                    cmdstr += ", CenterAddr='" + ModelItemIsNull(bc.CenterAddr) + "'";
                }
                if (bc.StationAddr != null)
                {
                    cmdstr += ", StationAddr='" + ModelItemIsNull(bc.StationAddr) + "'";
                }
                if (bc.Pwd != null)
                {
                    cmdstr += ",Pwd='" + ModelItemIsNull(bc.Pwd) + "'";
                }
                if (bc.MainChanlAddr1 != null)
                {
                    cmdstr += ", MainChanlAddr1='" + ModelItemIsNull(bc.MainChanlAddr1) + "'";
                }
                if (bc.BackChanlAddr1 != null)
                {
                    cmdstr += ", BackChanlAddr1='" + ModelItemIsNull(bc.BackChanlAddr1) + "'";
                }
                if (bc.MainChanlAddr2 != null)
                {
                    cmdstr += ", MainChanlAddr2='" + ModelItemIsNull(bc.MainChanlAddr2) + "'";
                }
                if (bc.BackChanlAddr2 != null)
                {
                    cmdstr += ", BackChanlAddr2='" + ModelItemIsNull(bc.BackChanlAddr2) + "'";
                }
                if (bc.MainChanlAddr3 != null)
                {
                    cmdstr += ", MainChanlAddr3='" + ModelItemIsNull(bc.MainChanlAddr3) + "'";
                }
                if (bc.BackChanlAddr3 != null)
                {
                    cmdstr += ", BackChanlAddr3='" + ModelItemIsNull(bc.BackChanlAddr3) + "'";
                }
                if (bc.MainChanlAddr4 != null)
                {
                    cmdstr += ", MainChanlAddr4='" + ModelItemIsNull(bc.MainChanlAddr4) + "'";
                }
                if (bc.BackChanlAddr4 != null)
                {
                    cmdstr += ", BackChanlAddr4='" + ModelItemIsNull(bc.BackChanlAddr4) + "'";
                }
                if (bc.WorkMode != null)
                {
                    cmdstr += ", WorkMode='" + ModelItemIsNull(bc.WorkMode) + "'";
                }
                if (bc.CollectCode != null)
                {
                    cmdstr += ", CollectCode='" + ModelItemIsNull(bc.CollectCode) + "'";
                }
                if (bc.SvrAddrRang != null)
                {
                    cmdstr += ", SvrAddrRang='" + ModelItemIsNull(bc.SvrAddrRang) + "'";
                }
                if (bc.CommDevID != null)
                {
                    cmdstr += ", CommDevID='" + ModelItemIsNull(bc.CommDevID) + "'";
                }
                if (bc.Sttyp != null)
                {
                    cmdstr += ", Sttyp='" + ModelItemIsNull(bc.Sttyp) + "'";
                }
                if (bc.SendMode != null)
                {
                    cmdstr += ", SendMode='" + ModelItemIsNull(bc.SendMode) + "'";
                }
                if (bc.Chmain1 != null)
                {
                    cmdstr += ", Chmain1='" + ModelItemIsNull(bc.Chmain1) + "'";
                }
                if (bc.Chmain2 != null)
                {
                    cmdstr += ", Chmain2='" + ModelItemIsNull(bc.Chmain2) + "'";
                }
                if (bc.Chmain3 != null)
                {
                    cmdstr += ", Chmain3='" + ModelItemIsNull(bc.Chmain3) + "'";
                }
                if (bc.Chmain4 != null)
                {
                    cmdstr += ", Chmain4='" + ModelItemIsNull(bc.Chmain4) + "'";
                }
                if (bc.Chback1 != null)
                {
                    cmdstr += ", Chback1='" + ModelItemIsNull(bc.Chback1) + "'";
                }
                if (bc.Chback2 != null)
                {
                    cmdstr += ", Chback2='" + ModelItemIsNull(bc.Chback2) + "'";
                }
                if (bc.Chback3 != null)
                {
                    cmdstr += ", Chback3='" + ModelItemIsNull(bc.Chback3) + "'";
                }
                if (bc.Chback4 != null)
                {
                    cmdstr += ", Chback4='" + ModelItemIsNull(bc.Chback4) + "'";
                }
                if (bc.GPRSsapn != null)
                {
                    cmdstr += ", GPRSsapn='" + ModelItemIsNull(bc.GPRSsapn) + "'";
                }
                if (bc.CenterIP1 != null)
                {
                    cmdstr += ", CenterIP1='" + ModelItemIsNull(bc.CenterIP1) + "'";
                }
                if (bc.CenterIP2 != null)
                {
                    cmdstr += ", CenterIP2='" + ModelItemIsNull(bc.CenterIP2) + "'";
                }
                if (bc.CenterIP3 != null)
                {
                    cmdstr += ", CenterIP3='" + ModelItemIsNull(bc.CenterIP3) + "'";
                }
                if (bc.CenterIP4 != null)
                {
                    cmdstr += ", CenterIP4='" + ModelItemIsNull(bc.CenterIP4) + "'";
                }
                if (bc.CenterPort1 != null)
                {
                    cmdstr += ", CenterPort1='" + ModelItemIsNull(bc.CenterPort1) + "'";
                }
                if (bc.CenterPort2 != null)
                {
                    cmdstr += ", CenterPort2='" + ModelItemIsNull(bc.CenterPort2) + "'";
                }
                if (bc.CenterPort3 != null)
                {
                    cmdstr += ", CenterPort3='" + ModelItemIsNull(bc.CenterPort3) + "'";
                }
                if (bc.CenterPort4 != null)
                {
                    cmdstr += ", CenterPort4='" + ModelItemIsNull(bc.CenterPort4) + "'";
                }
                cmdstr += "where StationID ='" + ModelItemIsNull(bc.StationID) + "' ";

                cmdstr += " end ";

                cmdstr += " else ";

                cmdstr += " begin ";

                cmdstr += " insert into BaseConfig(StationID ,TM,CenterAddr ,StationAddr ,Pwd ,MainChanlAddr1 ,BackChanlAddr1,MainChanlAddr2,BackChanlAddr2,MainChanlAddr3,BackChanlAddr3,MainChanlAddr4,BackChanlAddr4,WorkMode,CollectCode,SvrAddrRang,CommDevID,Sttyp,SendMode,Chmain1,Chmain2,Chmain3,Chmain4,Chback1,Chback2,Chback3,Chback4,GPRSsapn,CenterIP1,CenterIP2,CenterIP3,CenterIP4,CenterPort1,CenterPort2,CenterPort3,CenterPort4) values('" + ModelItemIsNull(bc.StationID) + "','" + ModelItemIsNull(bc.TM) + "','" + ModelItemIsNull(bc.CenterAddr) + "','" + ModelItemIsNull(bc.StationAddr) + "','" + ModelItemIsNull(bc.Pwd) + "','" + ModelItemIsNull(bc.MainChanlAddr1) + "' ,'" + ModelItemIsNull(bc.BackChanlAddr1) + "' ,'" + ModelItemIsNull(bc.MainChanlAddr2) + "','" + ModelItemIsNull(bc.BackChanlAddr2) + "','" + ModelItemIsNull(bc.MainChanlAddr3) + "','" + ModelItemIsNull(bc.BackChanlAddr3) + "','" + ModelItemIsNull(bc.MainChanlAddr4) + "','" + ModelItemIsNull(bc.BackChanlAddr4) + "','" + ModelItemIsNull(bc.WorkMode) + "','" + ModelItemIsNull(bc.CollectCode) + "','" + ModelItemIsNull(bc.SvrAddrRang) + "','" + ModelItemIsNull(bc.CommDevID) + "','" + ModelItemIsNull(bc.Sttyp) + "','" + ModelItemIsNull(bc.SendMode) + "','" + ModelItemIsNull(bc.Chmain1) + "','" + ModelItemIsNull(bc.Chmain2) + "','" + ModelItemIsNull(bc.Chmain3) + "','" + ModelItemIsNull(bc.Chmain4) + "','" + ModelItemIsNull(bc.Chback1) + "','" + ModelItemIsNull(bc.Chback2) + "','" + ModelItemIsNull(bc.Chback3) + "','" + ModelItemIsNull(bc.Chback4) + "','" + ModelItemIsNull(bc.GPRSsapn) + "','" + ModelItemIsNull(bc.CenterIP1) + "','" + ModelItemIsNull(bc.CenterIP2) + "','" + ModelItemIsNull(bc.CenterIP3) + "','" + ModelItemIsNull(bc.CenterIP4) + "','" + ModelItemIsNull(bc.CenterPort1) + "','" + ModelItemIsNull(bc.CenterPort2) + "','" + ModelItemIsNull(bc.CenterPort3) + "','" + ModelItemIsNull(bc.CenterPort4) + "')";

                cmdstr += " end ";
            }
            cmdstr = cmdstr.Replace("'null'", "null");

            int i = db.SqlExecute(cmdstr);
        }

        public void UpdateRunConfig(Model.HydroRunConfig rc)
        {
            string cmdstr = "";
            if (db.DbType == "MySql")
            {
                cmdstr += " insert into RunConfig(StationID,TM,SetMsgIntval ,AddMsgIntval ,RainStartDate ,CollectIntval ,WaterSaveIntval,RainResolution,WaterResolution,RainAddThod,WaterBase1,WaterBase2,waterFixBase1,WaterFixBase2,AddWater1,AddWater2,WaterUpThod,WaterDownThod,SendHour,Dtmin,DtSend,Picmin,Watermul1,Watermul2) values('" + ModelItemIsNull(rc.StationID) + "','" + ModelItemIsNull(rc.TM) + "','" + ModelItemIsNull(rc.SetMsgIntval) + "','" + ModelItemIsNull(rc.AddMsgIntval) + "','" + ModelItemIsNull(rc.RainStartDate) + "','" + ModelItemIsNull(rc.CollectIntval) + "' ,'" + ModelItemIsNull(rc.WaterSaveIntval) + "' ,'" + ModelItemIsNull(rc.RainResolution) + "','" + ModelItemIsNull(rc.WaterResolution) + "','" + ModelItemIsNull(rc.RainAddThod) + "' ,'" + ModelItemIsNull(rc.WaterBase1) + "' ,'" + ModelItemIsNull(rc.WaterBase2) + "','" + ModelItemIsNull(rc.waterFixBase1) + "' ,'" + ModelItemIsNull(rc.WaterFixBase2) + "' ,'" + ModelItemIsNull(rc.AddWater1) + "' ,'" + ModelItemIsNull(rc.AddWater2) + "','" + ModelItemIsNull(rc.WaterUpThod) + "' ,'" + ModelItemIsNull(rc.WaterDownThod) + "','" + ModelItemIsNull(rc.SendHour) + "','" + ModelItemIsNull(rc.Dtmin) + "','" + ModelItemIsNull(rc.DtSend) + "','" + ModelItemIsNull(rc.Picmin) + "','" + ModelItemIsNull(rc.Watermul1) + "','" + ModelItemIsNull(rc.Watermul2) + "' ) ";
                cmdstr += " on duplicate key update ";
                cmdstr += " StationID ='" + ModelItemIsNull(rc.StationID) + "'";

                if (rc.TM != null)
                {
                    cmdstr += ",TM='" + ModelItemIsNull(rc.TM) + "'";
                }
                if (rc.SetMsgIntval != null)
                {
                    cmdstr += ",SetMsgIntval='" + ModelItemIsNull(rc.SetMsgIntval) + "'";
                }
                if (rc.AddMsgIntval != null)
                {
                    cmdstr += ",AddMsgIntval='" + ModelItemIsNull(rc.AddMsgIntval) + "'";
                }
                if (rc.RainStartDate != null)
                {
                    cmdstr += ",RainStartDate='" + ModelItemIsNull(rc.RainStartDate) + "'";
                }
                if (rc.CollectIntval != null)
                {
                    cmdstr += ",CollectIntval='" + ModelItemIsNull(rc.CollectIntval) + "'";
                }
                if (rc.WaterSaveIntval != null)
                {
                    cmdstr += ",WaterSaveIntval='" + ModelItemIsNull(rc.WaterSaveIntval) + "'";
                }
                if (rc.RainResolution != null)
                {
                    cmdstr += ",RainResolution='" + ModelItemIsNull(rc.RainResolution) + "'";
                }
                if (rc.WaterResolution != null)
                {
                    cmdstr += ",WaterResolution='" + ModelItemIsNull(rc.WaterResolution) + "'";
                }
                if (rc.RainAddThod != null)
                {
                    cmdstr += ",RainAddThod='" + ModelItemIsNull(rc.RainAddThod) + "'";
                }
                if (rc.WaterBase1 != null)
                {
                    cmdstr += ",WaterBase1='" + ModelItemIsNull(rc.WaterBase1) + "'";
                }
                if (rc.WaterBase2 != null)
                {
                    cmdstr += ",WaterBase2='" + ModelItemIsNull(rc.WaterBase2) + "'";
                }
                if (rc.waterFixBase1 != null)
                {
                    cmdstr += ",waterFixBase1='" + ModelItemIsNull(rc.waterFixBase1) + "'";
                }
                if (rc.WaterFixBase2 != null)
                {
                    cmdstr += ",WaterFixBase2='" + ModelItemIsNull(rc.WaterFixBase2) + "'";
                }
                if (rc.AddWater1 != null)
                {
                    cmdstr += ",AddWater1='" + ModelItemIsNull(rc.AddWater1) + "'";
                }
                if (rc.AddWater2 != null)
                {
                    cmdstr += ",AddWater2='" + ModelItemIsNull(rc.AddWater2) + "'";
                }
                if (rc.WaterUpThod != null)
                {
                    cmdstr += ",WaterUpThod='" + ModelItemIsNull(rc.WaterUpThod) + "'";
                }
                if (rc.WaterDownThod != null)
                {
                    cmdstr += ",WaterDownThod='" + ModelItemIsNull(rc.WaterDownThod) + "'";
                }
                if (rc.SendHour != null)
                {
                    cmdstr += ",SendHour='" + ModelItemIsNull(rc.SendHour) + "'";
                }
                if (rc.Dtmin != null)
                {
                    cmdstr += ",Dtmin='" + ModelItemIsNull(rc.Dtmin) + "'";
                }
                if (rc.DtSend != null)
                {
                    cmdstr += ",DtSend='" + ModelItemIsNull(rc.DtSend) + "'";
                }
                if (rc.Picmin != null)
                {
                    cmdstr += ",Picmin='" + ModelItemIsNull(rc.Picmin) + "'";
                }
                if (rc.Watermul1 != null)
                {
                    cmdstr += ",Watermul1='" + ModelItemIsNull(rc.Watermul1) + "'";
                }
                if (rc.Watermul2 != null)
                {
                    cmdstr += ",Watermul2='" + ModelItemIsNull(rc.Watermul2) + "'";
                }
            }
            else
            {

                cmdstr += $"if exists(select 1 from RunConfig where StationID='{rc.StationID}') ";
                cmdstr += " begin ";
                cmdstr += " update RunConfig set StationID ='" + ModelItemIsNull(rc.StationID) + "'";

                if (rc.TM != null)
                {
                    cmdstr += ",TM='" + ModelItemIsNull(rc.TM) + "'";
                }
                if (rc.SetMsgIntval != null)
                {
                    cmdstr += ",SetMsgIntval='" + ModelItemIsNull(rc.SetMsgIntval) + "'";
                }
                if (rc.AddMsgIntval != null)
                {
                    cmdstr += ",AddMsgIntval='" + ModelItemIsNull(rc.AddMsgIntval) + "'";
                }
                if (rc.RainStartDate != null)
                {
                    cmdstr += ",RainStartDate='" + ModelItemIsNull(rc.RainStartDate) + "'";
                }
                if (rc.CollectIntval != null)
                {
                    cmdstr += ",CollectIntval='" + ModelItemIsNull(rc.CollectIntval) + "'";
                }
                if (rc.WaterSaveIntval != null)
                {
                    cmdstr += ",WaterSaveIntval='" + ModelItemIsNull(rc.WaterSaveIntval) + "'";
                }
                if (rc.RainResolution != null)
                {
                    cmdstr += ",RainResolution='" + ModelItemIsNull(rc.RainResolution) + "'";
                }
                if (rc.WaterResolution != null)
                {
                    cmdstr += ",WaterResolution='" + ModelItemIsNull(rc.WaterResolution) + "'";
                }
                if (rc.RainAddThod != null)
                {
                    cmdstr += ",RainAddThod='" + ModelItemIsNull(rc.RainAddThod) + "'";
                }
                if (rc.WaterBase1 != null)
                {
                    cmdstr += ",WaterBase1='" + ModelItemIsNull(rc.WaterBase1) + "'";
                }
                if (rc.WaterBase2 != null)
                {
                    cmdstr += ",WaterBase2='" + ModelItemIsNull(rc.WaterBase2) + "'";
                }
                if (rc.waterFixBase1 != null)
                {
                    cmdstr += ",waterFixBase1='" + ModelItemIsNull(rc.waterFixBase1) + "'";
                }
                if (rc.WaterFixBase2 != null)
                {
                    cmdstr += ",WaterFixBase2='" + ModelItemIsNull(rc.WaterFixBase2) + "'";
                }
                if (rc.AddWater1 != null)
                {
                    cmdstr += ",AddWater1='" + ModelItemIsNull(rc.AddWater1) + "'";
                }
                if (rc.AddWater2 != null)
                {
                    cmdstr += ",AddWater2='" + ModelItemIsNull(rc.AddWater2) + "'";
                }
                if (rc.WaterUpThod != null)
                {
                    cmdstr += ",WaterUpThod='" + ModelItemIsNull(rc.WaterUpThod) + "'";
                }
                if (rc.WaterDownThod != null)
                {
                    cmdstr += ",WaterDownThod='" + ModelItemIsNull(rc.WaterDownThod) + "'";
                }
                if (rc.SendHour != null)
                {
                    cmdstr += ",SendHour='" + ModelItemIsNull(rc.SendHour) + "'";
                }
                if (rc.Dtmin != null)
                {
                    cmdstr += ",Dtmin='" + ModelItemIsNull(rc.Dtmin) + "'";
                }
                if (rc.DtSend != null)
                {
                    cmdstr += ",DtSend='" + ModelItemIsNull(rc.DtSend) + "'";
                }
                if (rc.Picmin != null)
                {
                    cmdstr += ",Picmin='" + ModelItemIsNull(rc.Picmin) + "'";
                }
                if (rc.Watermul1 != null)
                {
                    cmdstr += ",Watermul1='" + ModelItemIsNull(rc.Watermul1) + "'";
                }
                if (rc.Watermul2 != null)
                {
                    cmdstr += ",Watermul2='" + ModelItemIsNull(rc.Watermul2) + "'";
                }
                cmdstr += " where StationID ='" + ModelItemIsNull(rc.StationID) + "' ";

                cmdstr += " end ";

                cmdstr += " else ";

                cmdstr += " begin ";

                cmdstr += " insert into RunConfig(StationID,TM,SetMsgIntval ,AddMsgIntval ,RainStartDate ,CollectIntval ,WaterSaveIntval,RainResolution,WaterResolution,RainAddThod,WaterBase1,WaterBase2,waterFixBase1,WaterFixBase2,AddWater1,AddWater2,WaterUpThod,WaterDownThod,SendHour,Dtmin,DtSend,Picmin,Watermul1,Watermul2) values('" + ModelItemIsNull(rc.StationID) + "','" + ModelItemIsNull(rc.TM) + "','" + ModelItemIsNull(rc.SetMsgIntval) + "','" + ModelItemIsNull(rc.AddMsgIntval) + "','" + ModelItemIsNull(rc.RainStartDate) + "','" + ModelItemIsNull(rc.CollectIntval) + "' ,'" + ModelItemIsNull(rc.WaterSaveIntval) + "' ,'" + ModelItemIsNull(rc.RainResolution) + "','" + ModelItemIsNull(rc.WaterResolution) + "','" + ModelItemIsNull(rc.RainAddThod) + "' ,'" + ModelItemIsNull(rc.WaterBase1) + "' ,'" + ModelItemIsNull(rc.WaterBase2) + "','" + ModelItemIsNull(rc.waterFixBase1) + "' ,'" + ModelItemIsNull(rc.WaterFixBase2) + "' ,'" + ModelItemIsNull(rc.AddWater1) + "' ,'" + ModelItemIsNull(rc.AddWater2) + "','" + ModelItemIsNull(rc.WaterUpThod) + "' ,'" + ModelItemIsNull(rc.WaterDownThod) + "','" + ModelItemIsNull(rc.SendHour) + "','" + ModelItemIsNull(rc.Dtmin) + "','" + ModelItemIsNull(rc.DtSend) + "','" + ModelItemIsNull(rc.Picmin) + "','" + ModelItemIsNull(rc.Watermul1) + "','" + ModelItemIsNull(rc.Watermul2) + "' ) ";

                cmdstr += " end ";

            }
            cmdstr = cmdstr.Replace("'null'", "null");

            int i = db.SqlExecute(cmdstr);
        }

        public bool BaseConfigExist(string StationID)
        {
            bool result = false;

            string cmdstr = "select * from BaseConfig where StationID ='" + StationID + "'";

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 30 * 24 * 60);

            if (dt.Rows.Count > 0)
            {
                result = true;
            }

            return result;

        }

        public bool RunConfigExist(string StationID)
        {
            bool result = false;

            string cmdstr = "select * from RunConfig where StationID ='" + StationID + "'";

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 30 * 24 * 60);

            if (dt.Rows.Count > 0)
            {
                result = true;
            }

            return result;

        }

        #endregion

        #region   遥测站状态、报警信息
        public void UpdateAlarmStatus(Model.HydroAlarmStatus has)
        {
            string cmdstr = "";
            if (db.DbType == "MySql")
            {
                cmdstr += "insert into AlarmStatus(StationID,ChargeStatus ,VolStatus ,WaterAlarmStatus ,FlowAlarmStatus ,QuaAlarmStatus,FlowStatus,"
                          + "WaterStatus,DoorStatus,MemStatus,IcStatus,PumpStatus,WatRemainStatus,RecvTime) values('"
                          + ModelItemIsNull(has.StationID) + "','" + ModelItemIsNull(has.ChargeStatus) + "','" + ModelItemIsNull(has.VolStatus) + "','"
                          + ModelItemIsNull(has.WaterAlarmStatus) + "','" + ModelItemIsNull(has.FlowAlarmStatus) + "' ,'" + ModelItemIsNull(has.QuaAlarmStatus)
                          + "' ,'" + ModelItemIsNull(has.FlowStatus) + "','" + ModelItemIsNull(has.WaterStatus) + "','" + ModelItemIsNull(has.DoorStatus)
                          + "' ,'" + ModelItemIsNull(has.MemStatus) + "' ,'" + ModelItemIsNull(has.IcStatus) + "','" + ModelItemIsNull(has.PumpStatus)
                          + "' ,'" + ModelItemIsNull(has.WatRemainStatus) + "' ,'" + ModelItemIsNull(has.RecvTime) + "' ) on duplicate key update ";

                cmdstr += " StationID ='" + ModelItemIsNull(has.StationID) + "'";

                if (has.ChargeStatus != char.MinValue)
                {
                    cmdstr += ",ChargeStatus= '" + ModelItemIsNull(has.ChargeStatus) + "'";
                }
                if (has.VolStatus != char.MinValue)
                {
                    cmdstr += ",VolStatus='" + ModelItemIsNull(has.VolStatus) + "'";
                }
                if (has.WaterAlarmStatus != char.MinValue)
                {
                    cmdstr += ",WaterAlarmStatus='" + ModelItemIsNull(has.WaterAlarmStatus) + "'";
                }
                if (has.FlowAlarmStatus != char.MinValue)
                {
                    cmdstr += ",FlowAlarmStatus='" + ModelItemIsNull(has.FlowAlarmStatus) + "'";
                }
                if (has.QuaAlarmStatus != char.MinValue)
                {
                    cmdstr += ",QuaAlarmStatus='" + ModelItemIsNull(has.QuaAlarmStatus) + "'";
                }
                if (has.FlowStatus != char.MinValue)
                {
                    cmdstr += ",FlowStatus='" + ModelItemIsNull(has.FlowStatus) + "'";
                }
                if (has.WaterStatus != char.MinValue)
                {
                    cmdstr += ",WaterStatus='" + ModelItemIsNull(has.WaterStatus) + "'";
                }

                if (has.DoorStatus != char.MinValue)
                {
                    cmdstr += ",DoorStatus='" + ModelItemIsNull(has.DoorStatus) + "'";
                }
                if (has.MemStatus != char.MinValue)
                {
                    cmdstr += ",MemStatus='" + ModelItemIsNull(has.MemStatus) + "'";
                }
                if (has.IcStatus != char.MinValue)
                {
                    cmdstr += ",IcStatus='" + ModelItemIsNull(has.IcStatus) + "'";
                }
                if (has.PumpStatus != char.MinValue)
                {
                    cmdstr += ",PumpStatus='" + ModelItemIsNull(has.PumpStatus) + "'";
                }
                if (has.WatRemainStatus != char.MinValue)
                {
                    cmdstr += ",WatRemainStatus='" + ModelItemIsNull(has.WatRemainStatus) + "'";
                }

                cmdstr += "RecvTime ='" + ModelItemIsNull(has.RecvTime) + "'";
            }
            else
            {
                cmdstr += $"if exists(select 1 from AlarmStatus where StationID='{ModelItemIsNull(has.StationID)}') ";
                cmdstr += " begin ";
                cmdstr += " update AlarmStatus set StationID ='" + ModelItemIsNull(has.StationID) + "'";

                if (has.ChargeStatus != char.MinValue)
                {
                    cmdstr += ",ChargeStatus= '" + ModelItemIsNull(has.ChargeStatus) + "'";
                }
                if (has.VolStatus != char.MinValue)
                {
                    cmdstr += ",VolStatus='" + ModelItemIsNull(has.VolStatus) + "'";
                }
                if (has.WaterAlarmStatus != char.MinValue)
                {
                    cmdstr += ",WaterAlarmStatus='" + ModelItemIsNull(has.WaterAlarmStatus) + "'";
                }
                if (has.FlowAlarmStatus != char.MinValue)
                {
                    cmdstr += ",FlowAlarmStatus='" + ModelItemIsNull(has.FlowAlarmStatus) + "'";
                }
                if (has.QuaAlarmStatus != char.MinValue)
                {
                    cmdstr += ",QuaAlarmStatus='" + ModelItemIsNull(has.QuaAlarmStatus) + "'";
                }
                if (has.FlowStatus != char.MinValue)
                {
                    cmdstr += ",FlowStatus='" + ModelItemIsNull(has.FlowStatus) + "'";
                }
                if (has.WaterStatus != char.MinValue)
                {
                    cmdstr += ",WaterStatus='" + ModelItemIsNull(has.WaterStatus) + "'";
                }

                if (has.DoorStatus != char.MinValue)
                {
                    cmdstr += ",DoorStatus='" + ModelItemIsNull(has.DoorStatus) + "'";
                }
                if (has.MemStatus != char.MinValue)
                {
                    cmdstr += ",MemStatus='" + ModelItemIsNull(has.MemStatus) + "'";
                }
                if (has.IcStatus != char.MinValue)
                {
                    cmdstr += ",IcStatus='" + ModelItemIsNull(has.IcStatus) + "'";
                }
                if (has.PumpStatus != char.MinValue)
                {
                    cmdstr += ",PumpStatus='" + ModelItemIsNull(has.PumpStatus) + "'";
                }
                if (has.WatRemainStatus != char.MinValue)
                {
                    cmdstr += ",WatRemainStatus='" + ModelItemIsNull(has.WatRemainStatus) + "'";
                }

                cmdstr += "RecvTime ='" + ModelItemIsNull(has.RecvTime) + "' where StationID ='" + ModelItemIsNull(has.StationID) + "' ";
                cmdstr += " end ";
                cmdstr += " else ";
                cmdstr += " begin ";
                cmdstr += " insert into AlarmStatus(StationID,ChargeStatus ,VolStatus ,WaterAlarmStatus ,FlowAlarmStatus ,QuaAlarmStatus,FlowStatus,"
               + "WaterStatus,DoorStatus,MemStatus,IcStatus,PumpStatus,WatRemainStatus,RecvTime) values('"
               + ModelItemIsNull(has.StationID) + "','" + ModelItemIsNull(has.ChargeStatus) + "','" + ModelItemIsNull(has.VolStatus) + "','"
               + ModelItemIsNull(has.WaterAlarmStatus) + "','" + ModelItemIsNull(has.FlowAlarmStatus) + "' ,'" + ModelItemIsNull(has.QuaAlarmStatus)
               + "' ,'" + ModelItemIsNull(has.FlowStatus) + "','" + ModelItemIsNull(has.WaterStatus) + "','" + ModelItemIsNull(has.DoorStatus)
               + "' ,'" + ModelItemIsNull(has.MemStatus) + "' ,'" + ModelItemIsNull(has.IcStatus) + "','" + ModelItemIsNull(has.PumpStatus)
               + "' ,'" + ModelItemIsNull(has.WatRemainStatus) + "' ,'" + ModelItemIsNull(has.RecvTime) + "' ) ";
                cmdstr += " end ";
            }

            cmdstr = cmdstr.Replace("'null'", "null");

            int i = db.SqlExecute(cmdstr);
        }

        public bool AlarmStatusExist(string StationID)
        {
            bool result = false;

            string cmdstr = "select * from AlarmStatus where StationID ='" + StationID + "'";

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 30 * 24 * 60);
            if (dt.Rows.Count > 0)
            {
                result = true;
            }

            return result;

        }
        #endregion

        #region 图像信息
        public object ModelItemIsNull(object str)
        {
            if (str == null || str.ToString().Trim().Length <= 0)
            {
                return "null";
            }
            else
            {
                return str;
            }
        }

        public int AddImgInfo(string stationID, DateTime time, string path)     //图像
        {
            string cmdstr = "insert into SK_IMGINFO_R(STCD ,TM ,VTDT) values('" + stationID + "','"
                + time + "','" + path + "')";

            int i = db.SqlExecute(cmdstr);

            return i;
        }

        public int AddVol(string stationID, DateTime time, float vol)     //电压
        {
            string cmdstr = "insert into SK_MACSTAT_R(STCD ,TM ,VOLTAGE) values('" + stationID + "','"
                + time + "'," + vol + ")";

            int i = db.SqlExecute(cmdstr);

            return i;
        }
        #endregion

        #region 更新接收报文数量

        public void AddTotalMsgStat(string stationID, string StationType, DateTime lastTime)
        {
            string cmdstr = "";

            if (db.DbType == "MySql")
            {
                cmdstr = " insert into TotalMsgStat(StationID,StationType,MsgTotal,LastMsgTime) values('" + stationID
                                                                                                          + "','" +
                                                                                                          StationType +
                                                                                                          "',1,'" +
                                                                                                          lastTime +
                                                                                                          "') on duplicate key update MsgTotal= MsgTotal +1,LastMsgTime = '" + lastTime +
                                                                                                          "' ";
            }
            else
            {
                cmdstr += $"if exists(select 1 from TotalMsgStat where StationID='{stationID}') ";
                cmdstr += " begin ";
                cmdstr += " update TotalMsgStat set MsgTotal= MsgTotal +1,LastMsgTime = '" + lastTime +
                         "' where StationID = '" + stationID + "'";
                cmdstr += " end ";
                cmdstr += " else ";
                cmdstr += " begin ";
                cmdstr += " insert into TotalMsgStat(StationID,StationType,MsgTotal,LastMsgTime) values('" + stationID
                                                                                                         + "','" +
                                                                                                         StationType +
                                                                                                         "',1,'" +
                                                                                                         lastTime +
                                                                                                         "') ";
                cmdstr += " end ";
            }

            int i = db.SqlExecute(cmdstr);
        }

        public void AddDayMsgStat(string stationID, int GPRS, int start, int safe, DateTime time)
        {
            string cmdstr = "";

            if (db.DbType == "MySql")
            {
                cmdstr = "insert into DayMsgStat(StationID,GPRSTotal,StartTotal,DayTotal,SafeMsg,TM) values('" + stationID
                                                                                                          + "'," +
                                                                                                          GPRS + "," +
                                                                                                          start +
                                                                                                          ",1," + safe +
                                                                                                          ",'" + time +
                                                                                                          "') on duplicate key update GPRSTotal= GPRSTotal +" + GPRS + ",StartTotal =StartTotal+" + start +
                                                                                                               " ,SafeMsg =SafeMsg+" + safe
                                                                                                               + ", DayTotal = DayTotal+1 ";
            }
            else
            {
                cmdstr += $"if exists(select 1 from DayMsgStat where StationID='{stationID}') ";
                cmdstr += " begin ";
                cmdstr += "update DayMsgStat set GPRSTotal= GPRSTotal +" + GPRS + ",StartTotal =StartTotal+" + start +
                          " ,SafeMsg =SafeMsg+" + safe
                          + ", DayTotal = DayTotal+1 where StationID = '" + stationID + "' and TM ='" + time + "'";
                cmdstr += " end ";
                cmdstr += " else ";
                cmdstr += " begin ";
                cmdstr += "insert into DayMsgStat(StationID,GPRSTotal,StartTotal,DayTotal,SafeMsg,TM) values('" + stationID
                                                                                                          + "'," +
                                                                                                          GPRS + "," +
                                                                                                          start +
                                                                                                          ",1," + safe +
                                                                                                          ",'" + time +
                                                                                                          "') ";
                cmdstr += " end ";
            }

            int i = db.SqlExecute(cmdstr);
        }

        public bool TotalMsgStatExist(string StationID)
        {
            bool result = false;

            string cmdstr = "select * from TotalMsgStat where StationID ='" + StationID + "'";

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 30 * 24 * 60);

            if (dt.Rows.Count > 0)
            {
                result = true;
            }

            return result;

        }

        public bool DayMsgStatExist(string StationID, DateTime time)
        {
            bool result = false;

            string cmdstr = "select * from DayMsgStat where StationID ='" + StationID + "' and TM ='" + time + "'";

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 12 * 60);

            if (dt.Rows.Count > 0)
            {
                result = true;
            }

            return result;

        }

        #endregion

        #region 更新实时数据
        public void UpdateLastData(Model.LastData data)
        {
            string cmdstr = "";
            if (db.DbType == "MySql")
            {
                cmdstr = "insert into LastData(FacNum,STCD,TM,PT,PJ,Z01,Z02,Z03,VT,SI,TP,SV,CV) values('"
                         + data.FacNum + "','" + data.STCD + "','" + data.TM + "','" + data.PT + "','" + data.PJ +
                         "','" + data.Z01 + "','" + data.Z02 + "','" + data.Z03 + "','" + data.VT + "','" + data.SI +
                         "','" + data.TP + "','" + data.SV + "','" + data.CV + "') on duplicate key update " +
                        "TM = '" + data.TM + "', PT = '" + data.PT + "',PJ = '" + data.PJ +
                         "',Z01 = '" + data.Z01 + "',Z02 = '" + data.Z02 + "',Z03 = '" + data.Z03 + "',VT = '" + data.VT
                         + "',SI = '" + data.SI + "',TP = '" + data.TP + "',SV = '" + data.SV + "',CV = '" + data.CV +
                         "'";
            }
            else
            {
                cmdstr += $"if exists(select 1 from LastData where stcd='{data.STCD}')";

                cmdstr += " begin ";

                cmdstr += " update LastData set TM = '" + data.TM + "', PT = '" + data.PT + "',PJ = '" + data.PJ +
                          "',Z01 = '" + data.Z01 + "',Z02 = '" + data.Z02 + "',Z03 = '" + data.Z03 + "',VT = '" +
                          data.VT
                          + "',SI = '" + data.SI + "',TP = '" + data.TP + "',SV = '" + data.SV + "',CV = '" + data.CV +
                          "' where STCD = '" + data.STCD + "' ";

                cmdstr += " end ";

                cmdstr += " else ";

                cmdstr += " begin ";

                cmdstr += " insert into LastData(FacNum,STCD,TM,PT,PJ,Z01,Z02,Z03,VT,SI,TP,SV,CV) values('"
                         + data.FacNum + "','" + data.STCD + "','" + data.TM + "','" + data.PT + "','" + data.PJ +
                         "','" + data.Z01 + "','" + data.Z02 + "','" + data.Z03 + "','" + data.VT + "','" + data.SI +
                         "','" + data.TP + "','" + data.SV + "','" + data.CV + "')";

                cmdstr += " end ";
            }

            cmdstr = cmdstr.Replace("'null'", "null");

            int i = db.SqlExecute(cmdstr);
        }

        public bool LastDataExist(string stcd)
        {
            bool result = false;

            string cmdstr = string.Format("select STCD from LastData where STCD = '{0}'", stcd);

            DataTable dt = LocalCache.Get(cmdstr, () => db.DataWorke(cmdstr), 30 * 24 * 60);

            if (dt != null && dt.Rows.Count > 0)
            {
                result = true;
            }
            return result;
        }
        #endregion

        #region 站点信息映射
        public string querySTCD(string StationID)
        {
            string STCD = "";

            string cmdstr = "SELECT SITECODE,WID,RID FROM siteinfo where SITECODE='" + StationID + "'";
            DataTable dt = db.DataWorke(cmdstr);

            if (dt.Rows.Count > 0)
            {
                STCD = dt.Rows[0][2].ToString();
            }
            else
            {
                STCD = StationID.PadLeft(8, '0');
            }

            return STCD;
        }
        #endregion


        public void ClusterEqueue()
        {
            db.ClusterEqueue();
            db.Dispose();
            db = null;
            //ThreadPool.QueueUserWorkItem(o => db.ClusterExecute());
            //Task.Factory.StartNew(() => db.ClusterExecute());
        }
    }
}
