﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class HydroBaseConfig
    {
        private string _StationID;           //  
        private DateTime _TM;
        private string _CenterAddr;          //  
        private string _StationAddr;         //  
        private string _Pwd;      

        private string _MainChanlAddr1;       //  
        private string _BackChanlAddr1;       //  
        private string _MainChanlAddr2;       //  
        private string _BackChanlAddr2;       //  
        private string _MainChanlAddr3;       //  
        private string _BackChanlAddr3;       //  
        private string _MainChanlAddr4;       //  
        private string _BackChanlAddr4;       //  

        private string _WorkMode;       
        private string _CollectCode;       
        private string _SvrAddrRang;      
        private string _CommDevID;

        private string _Sttyp;
        private string _SendMode;
        private string _Chmain1;
        private string _Chmain2;
        private string _Chmain3;
        private string _Chmain4;

        private string _Chback1;
        private string _Chback2;
        private string _Chback3;
        private string _Chback4;

        private string _GPRSsapn;

        private string _CenterIP1;
        private string _CenterIP2;
        private string _CenterIP3;
        private string _CenterIP4;

        private string _CenterPort1;
        private string _CenterPort2;
        private string _CenterPort3;
        private string _CenterPort4;


        public string StationID
        {
            get { return _StationID; }
            set { _StationID = value; }
        }
        public DateTime TM
        {
            get { return _TM; }
            set { _TM = value; }
        }
        public string CenterAddr
        {
            get { return _CenterAddr; }
            set { _CenterAddr = value; }
        }

        public string StationAddr
        {
            get { return _StationAddr; }
            set { _StationAddr = value; }
        }
        public string Pwd
        {
            get { return _Pwd; }
            set { _Pwd = value; }
        }

        public string MainChanlAddr1
        {
            get { return _MainChanlAddr1; }
            set { _MainChanlAddr1 = value; }
        }
        public string BackChanlAddr1
        {
            get { return _BackChanlAddr1; }
            set { _BackChanlAddr1 = value; }
        }
        public string MainChanlAddr2
        {
            get { return _MainChanlAddr2; }
            set { _MainChanlAddr2 = value; }
        }
        public string BackChanlAddr2
        {
            get { return _BackChanlAddr2; }
            set { _BackChanlAddr2 = value; }
        }

        public string MainChanlAddr3
        {
            get { return _MainChanlAddr3; }
            set { _MainChanlAddr3 = value; }
        }

        public string BackChanlAddr3
        {
            get { return _BackChanlAddr3; }
            set { _BackChanlAddr3 = value; }
        }

        public string MainChanlAddr4
        {
            get { return _MainChanlAddr4; }
            set { _MainChanlAddr4 = value; }
        }

        public string BackChanlAddr4
        {
            get { return _BackChanlAddr4; }
            set { _BackChanlAddr4 = value; }
        }
        public string WorkMode
        {
            get { return _WorkMode; }
            set { _WorkMode = value; }
        }
        public string CollectCode
        {
            get { return _CollectCode; }
            set { _CollectCode = value; }
        }
        public string SvrAddrRang
        {
            get { return _SvrAddrRang; }
            set { _SvrAddrRang = value; }
        }
        public string CommDevID
        {
            get { return _CommDevID; }
            set { _CommDevID = value; }
        }
        public string Sttyp
        {
            get { return _Sttyp; }
            set { _Sttyp = value; }
        }
        public string SendMode
        {
            get { return _SendMode; }
            set { _SendMode = value; }
        }
        public string Chmain1
        {
            get { return _Chmain1; }
            set { _Chmain1 = value; }
        }
        public string Chmain2
        {
            get { return _Chmain2; }
            set { _Chmain2 = value; }
        }
        public string Chmain3
        {
            get { return _Chmain3; }
            set { _Chmain3 = value; }
        }
        public string Chmain4
        {
            get { return _Chmain4; }
            set { _Chmain4 = value; }
        }

        public string Chback1
        {
            get { return _Chback1; }
            set { _Chback1 = value; }
        }
        public string Chback2
        {
            get { return _Chback2; }
            set { _Chback2 = value; }
        }
        public string Chback3
        {
            get { return _Chback3; }
            set { _Chback3 = value; }
        }
        public string Chback4
        {
            get { return _Chback4; }
            set { _Chback4 = value; }
        }
        public string GPRSsapn
        {
            get { return _GPRSsapn; }
            set { _GPRSsapn = value; }
        }
        public string CenterIP1
        {
            get { return _CenterIP1; }
            set { _CenterIP1 = value; }
        }
        public string CenterIP2
        {
            get { return _CenterIP2; }
            set { _CenterIP2 = value; }
        }
        public string CenterIP3
        {
            get { return _CenterIP3; }
            set { _CenterIP3 = value; }
        }
        public string CenterIP4
        {
            get { return _CenterIP4; }
            set { _CenterIP4 = value; }
        }
        public string CenterPort1
        {
            get { return _CenterPort1; }
            set { _CenterPort1 = value; }
        }
        public string CenterPort2
        {
            get { return _CenterPort2; }
            set { _CenterPort2 = value; }
        }
        public string CenterPort3
        {
            get { return _CenterPort3; }
            set { _CenterPort3 = value; }
        }
        public string CenterPort4
        {
            get { return _CenterPort4; }
            set { _CenterPort4= value; }
        }
    }
}
