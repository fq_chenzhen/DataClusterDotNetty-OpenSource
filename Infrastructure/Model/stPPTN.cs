﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    //降水量
    public class stPPTN
    {
        private string _stcd;         //测站编码
        private DateTime _tm;         //时间
        private float _drp;           //时段降水量
        private float _intv;          //时段长
        private float _pdr;           //降水历时
        private float _dyp;           //日降水量
        private char _wth;          //天气状况

        public string stcd
        {
            get { return _stcd; }
            set { _stcd = value; }
        }

        public DateTime tm
        {
            get { return _tm; }
            set { _tm = value; }
        }

        public float drp
        {
            get { return _drp; }
            set { _drp = value; }
        }

        public float intv
        {
            get { return _intv; }
            set { _intv = value; }
        }

        public float pdr
        {
            get { return _pdr; }
            set { _pdr = value; }
        }

        public float dyp
        {
            get { return _dyp; }
            set { _dyp = value; }
        }

        public char wth
        {
            get { return _wth; }
            set { _wth = value; }
        }
    }
}
