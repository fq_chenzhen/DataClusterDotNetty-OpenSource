﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    //河道水情
    public class stRIVER      
    {
        private string _stcd;        //测站编码
        private DateTime _tm;        //时间
        private float _z;            //水位
        private float _q;            //流量
        private float _xsa;          //断面过水面积
        private float _xsavv;        //断面平均流速
        private float _xsmxv;        //断面最大流速
        private char _flwchrcd;        //河水特征码
        private char _wptn;            //水势
        private char _msqmt;           //测流方法
        private char _msamt;           //测积方法
        private char _msvmt;           //测速方法

        public string stcd
        {
            get { return _stcd; }
            set { _stcd = value; }
        }

        public DateTime tm
        {
            get { return _tm; }
            set { _tm = value; }
        }

        public float z
        {
            get { return _z; }
            set { _z = value; }
        }

        public float q
        {
            get { return _q; }
            set { _q = value; }
        }

        public float xsa
        {
            get { return _xsa; }
            set { _xsa = value; }
        }

        public float xsavv
        {
            get { return _xsavv; }
            set { _xsavv = value; }
        }

        public float xsmxv
        {
            get { return _xsmxv; }
            set { _xsmxv = value; }
        }

        public char flwchrcd
        {
            get { return _flwchrcd; }
            set { _flwchrcd = value; }
        }

        public char wptn
        {
            get { return _wptn; }
            set { _wptn = value; }
        }
        public char msqmt
        {
            get { return _msqmt; }
            set { _msqmt = value; }
        }
        public char msamt
        {
            get { return _msamt; }
            set { _msamt = value; }
        }
        public char msvmt
        {
            get { return _msvmt; }
            set { _msvmt = value; }
        }
      
    }
}

