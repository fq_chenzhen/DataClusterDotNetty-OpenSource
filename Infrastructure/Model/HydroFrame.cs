﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class HydroFrame
    {
        private string _centerAddr;     //中心站地址
        private string _stationAddr;    //遥测站地址
        private string _pwd;           //密码
        private byte _funCode;        //功能码

        private int _total;    //包总数
        private int _no;       //包序号

        private int _serialNum;    //流水号
        private DateTime _sendMsgTime;  //发报时间
        private DateTime _observeTime;  //观测时间

        private string _stationClass;   //遥测站分类码标识符
        private string _type;           //参数类型标志
        private float _value;          //参数值
        private string  _strValue;          //参数值

        public string strValue
        {
            get { return _strValue; }
            set { _strValue = value; }
        }

        public string centerAddr
        {
            get { return _centerAddr; }
            set { _centerAddr = value; }
        }

        public string stationAddr
        {
            get { return _stationAddr; }
            set { _stationAddr = value; }
        }

        public string pwd
        {
            get { return _pwd; }
            set { _pwd = value; }
        }

        public byte funCode
        {
            get { return _funCode; }
            set { _funCode = value; }
        }

        public int total
        {
            get { return _total; }
            set { _total = value; }
        }

        public int no
        {
            get { return _no; }
            set { _no = value; }
        }

        public int serialNum
        {
            get { return _serialNum; }
            set { _serialNum = value; }
        }

        public DateTime sendMsgTime
        {
            get { return _sendMsgTime; }
            set { _sendMsgTime = value; }
        }

        public DateTime observeTime
        {
            get { return _observeTime; }
            set { _observeTime = value; }
        }

        public string stationClass
        {
            get { return _stationClass; }
            set { _stationClass = value; }
        }

        public string type
        {
            get { return _type; }
            set { _type = value; }
        }

        public float value
        {
            get { return _value; }
            set { _value = value; }
        }

    }
}
