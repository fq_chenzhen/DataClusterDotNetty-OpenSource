﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class HydroRunConfig
    {
        private string _StationID = null;           //  
        private DateTime _TM;
        private string _SetMsgIntval= null;         //  
        private string _AddMsgIntval= null;        //  
        private string _RainStartDate= null;

        private string _CollectIntval = null;       //  
        private string _WaterSaveIntval = null;       //  
        private string _RainResolution = null;       //  
        private string _WaterResolution = null;       //  
        private string _RainAddThod = null;       //  
        private string _WaterBase1 = null;       //  
        private string _WaterBase2 = null;       //  
        private string _waterFixBase1 = null;       //  

        private string _WaterFixBase2 = null;
        private string _AddWater1 = null;
        private string _AddWater2 = null;
        private string _WaterUpThod = null;
        private string _WaterDownThod = null;

        private string _SendHour;
        private string _Dtmin;
        private string _DtSend;
        private string _Picmin;
        private string _Watermul1;
        private string _Watermul2;


        public string StationID
        {
            get { return _StationID; }
            set { _StationID = value; }
        }
        public DateTime TM
        {
            get { return _TM; }
            set { _TM = value; }
        }
        public string SetMsgIntval
        {
            get { return _SetMsgIntval; }
            set { _SetMsgIntval = value; }
        }

        public string AddMsgIntval
        {
            get { return _AddMsgIntval; }
            set { _AddMsgIntval = value; }
        }
        public string RainStartDate
        {
            get { return _RainStartDate; }
            set { _RainStartDate = value; }
        }

        public string CollectIntval
        {
            get { return _CollectIntval; }
            set { _CollectIntval = value; }
        }
        public string WaterSaveIntval
        {
            get { return _WaterSaveIntval; }
            set { _WaterSaveIntval = value; }
        }
        public string RainResolution
        {
            get { return _RainResolution; }
            set { _RainResolution = value; }
        }
        public string WaterResolution
        {
            get { return _WaterResolution; }
            set { _WaterResolution = value; }
        }

        public string RainAddThod
        {
            get { return _RainAddThod; }
            set { _RainAddThod = value; }
        }

        public string WaterBase1
        {
            get { return _WaterBase1; }
            set { _WaterBase1 = value; }
        }

        public string WaterBase2
        {
            get { return _WaterBase2; }
            set { _WaterBase2 = value; }
        }

        public string waterFixBase1
        {
            get { return _waterFixBase1; }
            set { _waterFixBase1 = value; }
        }
        public string WaterFixBase2
        {
            get { return _WaterFixBase2; }
            set { _WaterFixBase2 = value; }
        }
        public string AddWater1
        {
            get { return _AddWater1; }
            set { _AddWater1 = value; }
        }
        public string AddWater2
        {
            get { return _AddWater2; }
            set { _AddWater2 = value; }
        }
        public string WaterUpThod
        {
            get { return _WaterUpThod; }
            set { _WaterUpThod = value; }
        }
        public string WaterDownThod
        {
            get { return _WaterDownThod; }
            set { _WaterDownThod = value; }
        }
        public string SendHour
        {
            get { return _SendHour; }
            set { _SendHour = value; }
        }
        public string Dtmin
        {
            get { return _Dtmin; }
            set { _Dtmin = value; }
        }
        public string DtSend
        {
            get { return _DtSend; }
            set { _DtSend = value; }
        }
        public string Picmin
        {
            get { return _Picmin; }
            set { _Picmin = value; }
        }
        public string Watermul1
        {
            get { return _Watermul1; }
            set { _Watermul1 = value; }
        }
        public string Watermul2
        {
            get { return _Watermul2; }
            set { _Watermul2= value; }
        }
    }
}
