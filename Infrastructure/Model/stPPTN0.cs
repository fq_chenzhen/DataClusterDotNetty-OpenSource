﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class stPPTN0
    {
        private string _stcd;         //测站编码
        private DateTime _tm;         //时间
        private float _val;           //雨量来报值
        private float _sub_val;       //降水量
        private char _err;           //错误标志
        private DateTime _GTM;           //
        

        public string stcd
        {
            get { return _stcd; }
            set { _stcd = value; }
        }

        public DateTime tm
        {
            get { return _tm; }
            set { _tm = value; }
        }

        public float val
        {
            get { return _val; }
            set { _val = value; }
        }

        public float sub_val
        {
            get { return _sub_val; }
            set { _sub_val = value; }
        }

        public char err
        {
            get { return _err; }
            set { _err = value; }
        }

        public DateTime GTM
        {
            get { return _GTM; }
            set { _GTM = value; }
        }
    }
}
