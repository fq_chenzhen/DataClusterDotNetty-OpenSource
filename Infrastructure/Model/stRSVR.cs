﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class stRSVR
    {
        private string _stcd;         //测站编码
        private DateTime _tm;         //时间
        private float _rz;            //库上水位
        private float _inq;           //入库流量
        private float _w;             //蓄水量
        private float _blrz;          //库下水位
        private float _otq;           //出库流量
        private char _rwchrcd;        //库水特征码
        private char _rwpin;          //库水水势
        private float _inqdr;         //入流时段长
        private char _msqmt;          //测流方法
       

        public string stcd
        {
            get { return _stcd; }
            set { _stcd = value; }
        }

        public DateTime tm
        {
            get { return _tm; }
            set { _tm = value; }
        }

        public float rz
        {
            get { return _rz; }
            set { _rz = value; }
        }

        public float inq
        {
            get { return _inq; }
            set { _inq = value; }
        }

        public float w
        {
            get { return _w; }
            set { _w = value; }
        }

        public float blrz
        {
            get { return _blrz; }
            set { _blrz = value; }
        }

        public float otq
        {
            get { return _otq; }
            set { _otq = value; }
        }

        public char rwchrcd
        {
            get { return _rwchrcd; }
            set { _rwchrcd = value; }
        }

        public char rwpin
        {
            get { return _rwpin; }
            set { _rwpin = value; }
        }

        public float inqdr
        {
            get { return _inqdr; }
            set { _inqdr = value; }
        }

        public char msqmt
        {
            get { return _msqmt; }
            set { _msqmt = value; }
        }
    }
}
