﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class HydroAlarmStatus
    {
        private string _StationID ;           //  
        private char _ChargeStatus ;         //  
        private char _VolStatus ;        //  
        private char _WaterAlarmStatus ;

        private char _FlowAlarmStatus ;       //  
        private char _QuaAlarmStatus;       //  
        private char _FlowStatus ;       //  
        private char _WaterStatus ;       //  
        private char _DoorStatus ;       //  
        private char _MemStatus ;       //  
        private char _IcStatus ;       //  

        private char _PumpStatus ;
        private char _WatRemainStatus ;
        private DateTime _RecvTime;
     


        public string StationID
        {
            get { return _StationID; }
            set { _StationID = value; }
        }

        public char ChargeStatus
        {
            get { return _ChargeStatus; }
            set { _ChargeStatus = value; }
        }

        public char VolStatus
        {
            get { return _VolStatus; }
            set { _VolStatus = value; }
        }
        public char WaterAlarmStatus
        {
            get { return _WaterAlarmStatus; }
            set { _WaterAlarmStatus = value; }
        }

        public char FlowAlarmStatus
        {
            get { return _FlowAlarmStatus; }
            set { _FlowAlarmStatus = value; }
        }
        public char QuaAlarmStatus
        {
            get { return _QuaAlarmStatus; }
            set { _QuaAlarmStatus = value; }
        }
        public char FlowStatus
        {
            get { return _FlowStatus; }
            set { _FlowStatus = value; }
        }
        public char WaterStatus
        {
            get { return _WaterStatus; }
            set { _WaterStatus = value; }
        }

        public char DoorStatus
        {
            get { return _DoorStatus; }
            set { _DoorStatus = value; }
        }

        public char MemStatus
        {
            get { return _MemStatus; }
            set { _MemStatus = value; }
        }

        public char IcStatus
        {
            get { return _IcStatus; }
            set { _IcStatus = value; }
        }

        public char PumpStatus
        {
            get { return _PumpStatus; }
            set { _PumpStatus = value; }
        }
        public char WatRemainStatus
        {
            get { return _WatRemainStatus; }
            set { _WatRemainStatus = value; }
        }

          public DateTime RecvTime
        {
            get { return _RecvTime; }
            set { _RecvTime = value; }
        }
      
    }
}
