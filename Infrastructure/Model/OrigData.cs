﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class OrigData
    {
        private string _stationID;   //站码
        private string _dataType;    //数据类型
        //private string _dataYWSJType;    //数据类型
        private string _datanewType;//最新数据表数据类型
        private string _chanal;      //信道
        private DateTime _datetime;  //数据时间
        private float _data;         //数据
        private string _MsgID;          //与报文相对应的ID
        private string facnum;

        public string stationID
        {
            get { return _stationID; }
            set { _stationID = value; }
        }

        public string MsgID
        {
            get { return _MsgID; }
            set { _MsgID = value; }
        }
        public string dataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }
        public string datanewType
        {
            get { return _datanewType; }
            set { _datanewType = value; }
        }
        public string chanal
        {
            get { return _chanal; }
            set { _chanal = value; }
        }

        public float data
        {
            get { return _data; }
            set { _data = value; }
        }

        public DateTime datetime
        {
            get { return _datetime; }
            set { _datetime = value; }
        }

        public string FacNum
        {
            get { return facnum; }
            set { facnum=value; }
        }
    }
}
