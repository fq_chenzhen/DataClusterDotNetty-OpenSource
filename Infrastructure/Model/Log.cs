﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Log
    {
        private string _logType;       //  类型
        private string _details;       //  说明
      
        private DateTime _recordTime;     //  记录时间
        private DateTime _processTime;    //  处理时间

        public string logType
        {
            get { return _logType; }
            set { _logType = value; }
        }

        public string details
        {
            get { return _details; }
            set { _details = value; }
        }

        public DateTime recordTime
        {
            get { return _recordTime; }
            set { _recordTime = value; }
        }

        public DateTime processTime
        {
            get { return _processTime; }
            set { _processTime = value; }
        }

    }
}
