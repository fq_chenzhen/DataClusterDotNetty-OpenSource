﻿using System;

namespace Model
{
    public class LastData
    {
        private string _facnum; //出厂编号
        private string _STCD;   //站码
        private DateTime _TM;  //数据时间
        private float _PT;    //数据类型
        private float _PJ;      //信道
        private float _Z01;
        private float _Z02;
        private float _Z03;
        private float _VT;
        private float _SI;          //数据
        private float _TP;          //数据
        private float _SV;
        private float _CV;

        public string FacNum
        {
            get { return _facnum; }
            set { _facnum = value; }
        }
        public string STCD
        {
            get { return _STCD; }
            set { _STCD = value; }
        }

        public DateTime TM
        {
            get { return _TM; }
            set { _TM = value; }
        }

        public float PT
        {
            get { return _PT; }
            set { _PT = value; }
        }

        public float PJ
        {
            get { return _PJ; }
            set { _PJ = value; }
        }

        public float Z01
        {
            get { return _Z01; }
            set { _Z01 = value; }
        }
        public float Z02
        {
            get { return _Z02; }
            set { _Z02 = value; }
        }
        public float Z03
        {
            get { return _Z03; }
            set { _Z03 = value; }
        }
        public float VT
        {
            get { return _VT; }
            set { _VT = value; }
        }

        public float SI
        {
            get { return _SI; }
            set { _SI = value; }
        }

        public float TP
        {
            get { return _TP; }
            set { _TP = value; }
        }
        public float SV
        {
            get { return _SV; }
            set { _SV = value; }
        }
        public float CV
        {
            get { return _CV; }
            set { _CV = value; }
        }
    }
}
