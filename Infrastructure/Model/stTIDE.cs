﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class stTIDE
    {
        private string _stcd;         //测站编码
        private DateTime _tm;         //时间
        private float _tdz;            //潮位
        private float _airp;           //气压
        private char _tdchrcd;             //潮水特征码
        private char _tdptn;          //潮势
        private char _hltdmk;           //高低潮标志


        public string stcd
        {
            get { return _stcd; }
            set { _stcd = value; }
        }

        public DateTime tm
        {
            get { return _tm; }
            set { _tm = value; }
        }

        public float tdz
        {
            get { return _tdz; }
            set { _tdz = value; }
        }

        public float airp
        {
            get { return _airp; }
            set { _airp = value; }
        }

        public char tdchrcd
        {
            get { return _tdchrcd; }
            set { _tdchrcd = value; }
        }

        public char tdptn
        {
            get { return _tdptn; }
            set { _tdptn = value; }
        }

        public char hltdmk
        {
            get { return _hltdmk; }
            set { _hltdmk = value; }
        }

        
    }
}
