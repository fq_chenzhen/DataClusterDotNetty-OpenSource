﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{

    public struct picInfo
    {
        public bool reSend;        //重新发送
        public string stationID;  //测站地址
        public string fileName;   //文件名
        public int frameTotal;    //图片数据包总数
        public List<int> frameNo;     //图片帧序号
        public List<byte[]> picDataTable;   //图片数据列表

        public int lastFrameNo;
    }

    public struct cmdInfo
    {
        public int id;             //数据库记录序号 ，当此记录指令下发后更改该记录状态
        public string stationID;    //站号
        public string cmd;         //指令
    }

    public class Command
    {
        private string _cmdType;       //  协议类型
        private string _cmdStr;        //  命令串
        private string _chanal;        //  信道
        private string _cmdFlag;       //  标志
         
        private DateTime _subTime;     //  提交时间
        private DateTime _processTime;  //  处理时间


        public string cmdType
        {
            get { return _cmdType; }
            set { _cmdType = value; }
        }

        public string cmdStr
        {
            get { return _cmdStr; }
            set { _cmdStr = value; }
        }

        public string chanal
        {
            get { return _chanal; }
            set { _chanal = value; }
        }

        public string cmdFlag
        {
            get { return _cmdFlag; }
            set { _cmdFlag = value; }
        }


        public DateTime subTime
        {
            get { return _subTime; }
            set { _subTime = value; }
        }

        public DateTime processTime
        {
            get { return _processTime; }
            set { _processTime = value; }
        }

    }
}
