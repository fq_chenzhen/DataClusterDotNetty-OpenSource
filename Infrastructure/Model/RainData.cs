﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class RainData
    {
        private string _facnum;     //出厂编号
        private string _stationID;   //站码
        private string _dataType;    //数据类型
        private float _AverAdd;
        private float _data01;         //数据
        private float _data02;         //数据
        private float _data03;         //数据
        private float _data04;         //数据
        private float _data05;         //数据
        private float _data06;         //数据
        private float _data07;         //数据
        private float _data08;         //数据
        private float _data09;         //数据
        private float _data10;         //数据
        private float _data11;         //数据
        private float _data12;         //数据
        private DateTime _datetime;  //数据时间
        private string _chanal;      //信道

        public string FacNum
        {
            get { return _facnum; }
            set { _facnum = value; }
        }
        public string stationID
        {
            get { return _stationID; }
            set { _stationID = value; }
        }
        public string dataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }
        public float AverAdd
        {
            get { return _AverAdd; }
            set { _AverAdd = value; }
        }
        public float data01
        {
            get { return _data01; }
            set { _data01 = value; }
        }
        public float data02
        {
            get { return _data02; }
            set { _data02 = value; }
        }
        public float data03
        {
            get { return _data03; }
            set { _data03 = value; }
        }
        public float data04
        {
            get { return _data04; }
            set { _data04 = value; }
        }
        public float data05
        {
            get { return _data05; }
            set { _data05 = value; }
        }
        public float data06
        {
            get { return _data06; }
            set { _data06 = value; }
        }
        public float data07
        {
            get { return _data07; }
            set { _data07 = value; }
        }
        public float data08
        {
            get { return _data08; }
            set { _data08 = value; }
        }
        public float data09
        {
            get { return _data09; }
            set { _data09 = value; }
        }
        public float data10
        {
            get { return _data10; }
            set { _data10 = value; }
        }
        public float data11
        {
            get { return _data11; }
            set { _data11 = value; }
        }
        public float data12
        {
            get { return _data12; }
            set { _data12 = value; }
        }
        public DateTime datetime
        {
            get { return _datetime; }
            set { _datetime = value; }
        }
        public string chanal
        {
            get { return _chanal; }
            set { _chanal = value; }
        }
    }
}
