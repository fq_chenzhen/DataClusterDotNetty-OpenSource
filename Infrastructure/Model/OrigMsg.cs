﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class OrigMsg
    {
        private string _stationID;   //站码
        private string _msg;        //报文内容
        private DateTime _recvTime;  //报文接收时间
        private string _ip;
        private string _facnum; //出厂编号

        public string stationID
        {
            get { return _stationID; }
            set { _stationID = value; }
        }

        public string msg
        {
            get { return _msg; }
            set { _msg = value; }
        }

        public DateTime recvTime
        {
            get { return _recvTime; }
            set { _recvTime = value; }
        }
        public string ip
        {
            get { return _ip; }
            set { _ip = value; }
        }

        public string FacNum
        {
            get { return _facnum; }
            set { _facnum = value; }
        }
    }
}
