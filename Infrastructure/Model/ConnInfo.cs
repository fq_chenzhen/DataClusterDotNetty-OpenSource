﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class ConnInfo
    {
        private string _dataPort;
        private string _localIP;
        private string _dbconstr;

        private string _dbconstr2;
        private string _dbconstr3;

        private string _sendPort;

        public string dataPort
        {
            get { return _dataPort; }
            set { _dataPort = value; }
        }

        //public string dataYWSJType
        //{
        //    get { return _dataYWSJType; }
        //    set { _dataYWSJType = value; }
        //}
        public string localIP
        {
            get { return _localIP; }
            set { _localIP = value; }
        }
        public string dbconstr
        {
            get { return _dbconstr; }
            set { _dbconstr = value; }
        }
        public string dbconstr2
        {
            get { return _dbconstr2; }
            set { _dbconstr2 = value; }
        }

        public string dbconstr3
        {
            get { return _dbconstr3; }
            set { _dbconstr3 = value; }
        }

        public string sendPort
        {
            get { return _sendPort; }
            set { _sendPort = value; }
        }
    }
}
