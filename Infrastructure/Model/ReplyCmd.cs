﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class ReplyCmd
    {
        private string _STCD;   //站码
        private DateTime _TM;  //数据时间
        private string _CmdStr;    //数据类型
        public string STCD
        {
            get { return _STCD; }
            set { _STCD = value; }
        }

        public DateTime TM
        {
            get { return _TM; }
            set { _TM = value; }
        }

        public string CmdStr
        {
            get { return _CmdStr; }
            set { _CmdStr = value; }
        }
    }
}
