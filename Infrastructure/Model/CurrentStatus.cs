﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
     public class CurrentStatus
    {
        private string _stationID;   //站码
        private string _mode;        //传输信道
        private string _modeBak;     //备用信道
        private string _hour;        //定时传输时间
        private string _dataS1;     //雨量定量传输量
        private string _dataS2;        //水位定量传输量
        private string _time1;     //水位采集时间
        private string _time2;        //风情采集时间
        private string _number1;     //本机号码
        private string _number2;        //本机号码
        private string _number3;     //本机号码
        private string _number4;        //本机号码
        private string _number5;     //本机号码
        private string _IP1;        //服务器地址
        private string _IP2;     //服务器地址
        private string _IP3;        //服务器地址
        private string _IP4;     //服务器地址
        private string _IP5;        //服务器地址

        public string stationID
        {
            get { return _stationID; }
            set { _stationID = value; }
        }

        public string mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        public string modeBak
        {
            get { return _modeBak; }
            set { _modeBak = value; }
        }

        public string hour
        {
            get { return _hour; }
            set { _hour = value; }
        }

        public string dataS1
        {
            get { return _dataS1; }
            set { _dataS1 = value; }
        }

        public string dataS2
        {
            get { return _dataS2; }
            set { _dataS2 = value; }
        }

        public string time1
        {
            get { return _time1; }
            set { _time1 = value; }
        }
        public string time2
        {
            get { return _time2; }
            set { _time2 = value; }
        }
        // 
        public string number1
        {
            get { return _number1; }
            set { _number1 = value; }
        }
        public string number2
        {
            get { return _number2; }
            set { _number2 = value; }
        }
        public string number3
        {
            get { return _number3; }
            set { _number3 = value; }
        }
        public string number4
        {
            get { return _number4; }
            set { _number4 = value; }
        }
        public string number5
        {
            get { return _number5; }
            set { _number5 = value; }
        }

        public string IP1
        {
            get { return _IP1; }
            set { _IP1 = value; }
        }
        public string IP2
        {
            get { return _IP2; }
            set { _IP2 = value; }
        }
        public string IP3
        {
            get { return _IP3; }
            set { _IP3 = value; }
        }
        public string IP4
        {
            get { return _IP4; }
            set { _IP4 = value; }
        }
        public string IP5
        {
            get { return _IP5; }
            set { _IP5 = value; }
        }
    }
}
