﻿using log4net;
using log4net.Config;
using log4net.Repository;
using System.IO;
using System.Reflection;

namespace SysLog
{
    public class LogHelper
    {
        private static ILoggerRepository repository { get; set; }
        private static ILog _log;
        private static object _lockObj = new object();
        private static ILog log
        {
            get
            {
                if (_log == null)
                {
                    lock (_lockObj)
                    {
                        if (_log == null)
                        {
                            Configure();
                        }
                    }
                }
                return _log;
            }
        }

        public static void Configure(string repositoryName = "NETCoreRepository", string configFile = "log4net.config")
        {
            //string assemblyFilePath = Assembly.GetExecutingAssembly().Location;
            //string assemblyDirPath = Path.GetDirectoryName(assemblyFilePath);
            //string configFilePath = assemblyDirPath + "\\" + configFile;
            repository = LogManager.CreateRepository(repositoryName);
            XmlConfigurator.Configure(repository, new FileInfo(configFile));
            _log = LogManager.GetLogger(repositoryName, "");
        }

        public static void Debug(string msg)
        {
            log.Debug(msg);
        }

        public static void Info(string msg)
        {
            log.Info(msg);
        }

        public static void Warn(string msg)
        {
            log.Warn(msg);
        }

        public static void Error(string msg)
        {
            log.Error(msg);
        }

        public static void Fatal(string msg)
        {
            log.Fatal(msg);
        }
    }
}
