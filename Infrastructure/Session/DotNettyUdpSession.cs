﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Coldairarrow.DotNettySocket;

namespace Infrastructure
{
    public class DotNettyUdpSession : ISession
    {
        private readonly IUdpSocket _udpSocket;
        private readonly EndPoint _endPoint;

        public IPEndPoint RemoteEndPoint => (IPEndPoint)_endPoint;


        public DotNettyUdpSession(IUdpSocket udpSocket, EndPoint endPoint)
        {
            _udpSocket = udpSocket ?? throw new ArgumentNullException(nameof(udpSocket));
            _endPoint = endPoint;
        }

        public void Send(byte[] data, int offset, int length)
        {
            _udpSocket.Send(data, _endPoint);
        }

        public void Send(string message)
        {
            byte[] bytes = System.Text.Encoding.Default.GetBytes(message);
            _udpSocket.Send(bytes, _endPoint);
        }
    }
}
