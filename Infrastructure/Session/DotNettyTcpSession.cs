﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Coldairarrow.DotNettySocket;

namespace Infrastructure
{
    public class DotNettyTcpSession : ISession
    {
        private readonly ITcpSocketConnection _tcpSocketConnection;
        public IPEndPoint RemoteEndPoint => _tcpSocketConnection.ClientAddress;

        public DotNettyTcpSession(ITcpSocketConnection tcpSocketConnection)
        {
            _tcpSocketConnection = tcpSocketConnection;
        }

        public void Send(byte[] data, int offset, int length)
        {
            _tcpSocketConnection.Send(data);
        }

        public void Send(string message)
        {
            byte[] bytes = System.Text.Encoding.Default.GetBytes(message);
            _tcpSocketConnection.Send(bytes);
        }
    }
}
