﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Infrastructure
{
    public interface ISession
    {
        IPEndPoint RemoteEndPoint { get; }

        void Send(byte[] data, int offset, int length);

        void Send(string message);
    }
}
