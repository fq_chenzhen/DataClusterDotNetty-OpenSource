﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Checkout
{

    public class Check
    {
        byte[] aucCRCHi = new byte[] {
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40
        };

        byte[] aucCRCLo = new byte[] {
            0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,
            0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
            0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,
            0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
            0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
            0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
            0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,
            0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
            0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,
            0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
            0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,
            0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
            0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB,
            0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
            0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
            0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
            0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,
            0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
            0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,
            0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
            0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,
            0x41, 0x81, 0x80, 0x40
        };

        public ushort GetCRC16(byte[] pucFrame,int start, int usLen)   //16 位CRC校验
        {
            byte ucCRCHi = 0xFF;
            byte ucCRCLo = 0xFF;
            int iIndex;
            int i = start;
            while (usLen-- > 0)
            {
                iIndex = ucCRCLo ^ pucFrame[i++];
                ucCRCLo = (byte)(ucCRCHi ^ aucCRCHi[iIndex]);
                ucCRCHi = aucCRCLo[iIndex];
            }
            return (ushort)(ucCRCHi << 8 | ucCRCLo);
        }

        //多项式为X7+X6+X5+X2+1,对应二进制为11100101,对应十六进制为0xE5
        public byte GetCRC8(byte[] data,int startIndex, int lenth)
        {
            byte CRC_Register = 0x00;		//初始值为0
            int Bit_Move = 0;
            while (lenth-- > 0)
            {
                CRC_Register = (byte)(CRC_Register ^ data[startIndex++]);

                for (Bit_Move = 8; Bit_Move > 0; Bit_Move--)
                {
                    if ((CRC_Register & 0x80) == 0x80)
                        CRC_Register = (byte)((CRC_Register << 1) ^ 0xE5);	//多项式值为E5,被校验值左移
                    else
                        CRC_Register = (byte)(CRC_Register << 1);
                }
            }
            return CRC_Register;
        }


        public ushort GetXOR16(byte[] data,int start,int end)          //16 位异或校验
        {
            byte xorL = 0x00;
            byte xorH = 0x00;
            for (int i = start; i < end;i++ )
            {
                xorH = (byte)(xorH^data[i]);
                xorL = (byte)(xorL^data[i+1]);
                i++;
            }
            return (ushort)(xorH << 8 | xorL);
        }

        public byte GetXOR8(byte[] data, int start, int end)              //8 位异或校验
        {
            byte xor8 = 0x00;
            for (int i = start; i < end; i++)
            {
                xor8 = (byte)(xor8 ^ data[i]);
            }
            return xor8;
        }

        public byte GetCS(byte[] data, int start, int end)         //8 位和校验
        {
            byte CS = 0x00;
            if (data != null)
            {
                for (int i = start; i < end; i++)
                {
                    CS += data[i];
                }
            }
            return CS;
        }

        public string byteToHexStr(byte[] bytes)
        {
            string hexStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    hexStr += bytes[i].ToString("X2");
                }
            }
            return hexStr;
        }
        //
        //index 下标   n 元素个数
        //
        //
        public string byteToHexStr(byte[] bytes , int index ,int n)
        {
            string hexStr = "";
            if (bytes != null && (index + n) <= bytes.Length)
            {
                for (int i = index; i < index + n; i++)
                {
                    hexStr += bytes[i].ToString("X2");
                }
            }
            return hexStr;
        }

        public byte GetBCH(byte[] data,int index , uint n)
        {
            byte[] pnt = new byte[20];
            byte[] tt = new byte[20];
            int i;
            int j;
            int temp;

            for (i = 0; i < n; i++)
            {
                pnt[i] = data[index + i];
            }

            pnt[n] = 0;

            for (i = 0; i < n + 1; i++)
            {
                tt[i] = pnt[i];//give initial value 
            }

            for (i = 0; i < (8 * n + 1); i++)
            {
                if ((byte)(tt[0] & 0x80) > 0)
                {
                    tt[0] = (byte)(tt[0] ^ 0xe5);
                }
                for (j = 0; j < n; j++)
                {
                    tt[j] = (byte)(tt[j] << 1);
                    if ((byte)(tt[j + 1] & 0x80) > 0)
                    {
                        tt[j]++;
                    }
                }
            }
            pnt[n] = (byte)(tt[0] & 0xfe);

            for (i = 0; i < n + 1; i++) tt[i] = pnt[i];
            temp = 0;
            for (j = 0; j < n + 1; j++)
            {
                for (i = 0; i < 8; i++)
                {
                    if ((byte)(tt[j] & 0x80) > 0) temp++;
                    tt[j] = (byte)(tt[j] << 1);
                }
            }
            if (temp % 2 > 0)
                pnt[n] = (byte)(pnt[n] | 0x01);
            else
                pnt[n] = (byte)(pnt[n] & 0xfe);    //end of get BCH  return pnt[n];   

            return (pnt[n]);
        }

        //16进制格式string 转byte[]：
        public byte[] HexStrToBytes(string hexString)
        {            
  
            if (hexString.Length % 2 != 0)
            {                  
                 hexString = "0" + hexString; 
            }

            int byteLength = hexString.Length / 2;
            byte[] bytes = new byte[byteLength];

            for (int i=0; i<byteLength; i++)
            {
                 bytes[i] = Convert.ToByte(hexString.Substring(i*2 , 2),16);           
            }

            return bytes;       
         }

        //判断是否只包含数字
        public bool IsNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]\d*$");
            return reg1.IsMatch(str);
        }
    }
}
