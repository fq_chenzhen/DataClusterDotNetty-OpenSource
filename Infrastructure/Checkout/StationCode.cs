﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Checkout
{
    public class StationCode
    {

        //降水	        50H	P
        //河道	        48H	H
        //水库(湖泊)	4BH	K
        //闸坝	        5AH	Z
        //泵站	        44H	D
        //潮汐	        54H	T
        //墒情	        4DH	M
        //地下水	    47H	G
        //水质	        51H	Q
        //取水口	    49H	I
        //排水口	    4FH	O


        //遥测站分类码Hex编码
        byte[] typeCode_Hex = new byte[]    
        {
            0x50,0x48,0x4B,0X5A,0X44,0X54,0X4D,0X47,0X51,0X49,0X4F
        };
        //遥测站分类码ASCII编码
        char[] typeCode_ASCII = new char[] 
        {
            'P','H','K','Z','D','T','M','G','Q','I','O'
        };


        //编码要数标识符ASCII码
        //下标从01H -> 75H
        string[] InfoCode_ASCII  = new string[]    
        {
           "AC","AI","C","DRxnn","DT","ED","EJ","FL","GH","GN","GS",
           "GT","GTP","H","HW","M10","M20","M30","M40","M50","M60",
           "M80","M100","MST","NS","P01","P02","P03","P06","P12","PD","PJ",
           "PN01","PN05","PN10","PN30","PR","PT","Q01","Q01","Q02","Q03",
           "Q04","Q05","Q06","Q07","Q08","QA","QZ","SW","UC","UE","US",
           "VA","VJ","BV","Z01","ZB","ZU","Z01","Z02","Z03","Z04","Z05",
           "Z06","Z07","Z08","SQ","ZT","pH","DO","COND","TURB","CODMN",
           "REDOX","NH4N","TP","TN","TOC","CU","ZN","SE","AS","THG",
           "CD","PB","CHLA","WP1","WP2","WP3","WP4","WP5","WP6","WP7",
           "WP8","SYL1","SYL2","SYL3","SYL4","SYL5","SYL6","SYL7",
           "SYL8","SBL1","SBL2","SBL3","SBL4","SBL5","SBL6","SBL7"
        };

        string[] InfoCode_7A_ASCII = new string[]
        {
            "SI"
        };

        //编码要素及标识符ASCII码
        //下标从F0H -> FDH
        string[] InfoCode_F_ASCII = new string[]
        {
            "TT","ST","RGZS","PIC","PN05","ZN05","DRZ3","DRZ4","DRZ5","DRZ6","DRZ7","DRZ8","DATA"

        };

        //编码要素及标识符ASCII码     用户自定义
        //下标从A0H开始
        string[] InfoCode_A_ASCII = new string[]
        {
            "TP"
        };
        string[] InfoCode1_A_ASCII = new string[]
        {
            "SV"
        };
        string[] InfoCode2_A_ASCII = new string[]
        {
            "CV"
        };
        string[] InfoCode3_A_ASCII = new string[]
        {
            "LD"
        };
        public char GetTypeCode_ASCII(byte code)
        {
            for (int i = 0; i < typeCode_Hex.Length; i++)
            {
                if (typeCode_Hex[i] == code)
                {
                    return typeCode_ASCII[i];
                }
            }
            
            return  '1';  //错误
        }


        public string GetInfo_ASCII(byte code)
        {
            if (code > 0x00 && code < 0x76)
            {
                return InfoCode_ASCII[code -1];
            }

            if (code==0x7A)
            {
                return InfoCode_7A_ASCII[0];
            }
            else if(code >=0xF0 && code <0xFE)
            {
                return InfoCode_F_ASCII[code - 0xF0];
            }

            else if (code == 0xA0)
            {
                return InfoCode_A_ASCII[0];
            }
            else if (code == 0xA1)
            {
                return InfoCode1_A_ASCII[0];
            }
            else if (code == 0xA2)
            {
                return InfoCode2_A_ASCII[0];
            }
            else if(code == 0xA3)
            {
                return InfoCode3_A_ASCII[0];
            }
            else
            {
                return "";
            }
        }

        
    }
}
